from django.contrib.auth import authenticate
import json
import os
import requests
from functools import wraps
import jwt
from jwt.exceptions import DecodeError
from django.http import JsonResponse

import logging
# Get an instance of a logger
logger = logging.getLogger('django')


def jwt_get_username_from_payload_handler(payload):
    print('payload', payload)
    username = payload.get('sub').replace('|', '.')
    authenticate(remote_user=username)
    return username


def jwt_decode_token(token):
    header = jwt.get_unverified_header(token)
    jwks = requests.get('https://{}/.well-known/jwks.json'
                        .format(os.environ.get('KAMVILLE_AUTH0_DOMAIN'))).json()
    public_key = None
    for jwk in jwks['keys']:
        if jwk['kid'] == header['kid']:
            public_key = jwt.algorithms.RSAAlgorithm.from_jwk(json.dumps(jwk))

    if public_key is None:
        raise Exception('Public key not found.')

    issuer = 'https://{}/'.format(os.environ.get('KAMVILLE_AUTH0_DOMAIN'))

    return jwt.decode(token, public_key,
                      audience=os.environ.get('KAMVILLE_AUTH0_AUDIENCE'),
                      issuer=issuer,
                      algorithms=['RS256'])


def get_token_auth_header(request):
    """Obtains the Access Token from the Authorization Header
    """
    auth = request.META.get("HTTP_AUTHORIZATION", None)
    parts = auth.split()
    token = parts[1]
    return token


def requires_scope(required_scope):
    """Determines if the required scope is present in the Access Token
    Args:
        required_scope (str): The scope required to access the resource
    """
    def require_scope(f):
        @wraps(f)
        def decorated(*args, **kwargs):
                token = get_token_auth_header(args[0])
                decoded = jwt.decode(token, verify=False)
                if decoded.get("scope"):
                    token_scopes = decoded["scope"].split()
                    for token_scope in token_scopes:
                        if token_scope == required_scope:
                            return f(*args, **kwargs)
                response = JsonResponse({'message': 'You don\'t have access to this resource'})
                response.status_code = 403
                return response
        return decorated
    return require_scope


def requires_permission(required_permission):
    """Determines if the required permission is present in the Access Token
    Args:
        required_permission (str): The permission required to access the resource
    """
    def require_permission(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            token = get_token_auth_header(args[0])
            decoded = jwt.decode(token, verify=False)

            for token_permission in decoded.get('permissions'):
                if token_permission == required_permission:
                    return f(*args, **kwargs)
            response = JsonResponse({'message': 'You don\'t have access to this resource'})
            response.status_code = 403
            return response
        return decorated
    return require_permission

def get_email(request, email_query_param="customer"):
    """Obtains the email from the request query params
    """
    email = request.query_params.get(email_query_param, None)
    return email


def requires_scope_or_owner(required_scope, owner_fields=('customer', 'email',)):
    """Determines if the required scope is present in the Access Token
    Args:
        required_scope (str): The scope required to access the resource
        owner_fields (tuple or list): An array of instance fields that lead to
        the email field in the users model
    """
    def require_scope(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            instance = args[0]
            token = get_token_auth_header(instance.request)
            request_owner = get_email(instance.request, owner_fields[0])

            try:
                instance_owner = instance.get_object()
            except AssertionError as e:
                instance_owner = instance.get_queryset()[0]

            for field in owner_fields:
                if instance_owner:
                    try:
                        instance_owner = getattr(instance_owner, field)
                    except Exception as e:
                        logger.error("Failed to retrieve "
                                     "foreign key object in utils.py\n", e)
                        break
                else:
                    break
            decoded = jwt.decode(token, verify=False)
            if decoded.get("scope"):
                token_scopes = decoded["scope"].split()
                for token_scope in token_scopes:
                    if token_scope == required_scope or \
                            request_owner == instance_owner:
                        return f(*args, **kwargs)
            response = JsonResponse({'message': 'You don\'t have access to this resource'})
            response.status_code = 403
            return response
        return decorated
    return require_scope


def requires_permission_or_owner(required_permission, owner_fields=('customer', 'email',)):
    """Determines if the required permission is present in the Access Token
    Args:
        required_permission (str): The permission required to access the resource
        owner_fields (tuple or list): An array of instance fields that lead to
        the email field in the users model
    """
    def require_permission(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            instance = args[0]
            token = get_token_auth_header(instance.request)
            request_owner = get_email(instance.request, owner_fields[0])

            try:
                instance_owner = instance.get_object()
            except AssertionError as e:
                instance_owner = instance.get_queryset()[0]

            for field in owner_fields:
                if instance_owner:
                    try:
                        instance_owner = getattr(instance_owner, field)
                    except Exception as e:
                        logger.error("Failed to retrieve "
                                     "foreign key object in utils.py\n", e)
                        break
                else:
                    break
            try:
                decoded = jwt.decode(token, verify=False)

                for permission in decoded.get('permissions'):
                    print('permission', permission)
                    if permission == required_permission or \
                            request_owner == instance_owner:
                        return f(*args, **kwargs)
            except DecodeError as e:
                print('Token decode error', e)
                response = JsonResponse({'message': 'You don\'t have access to this resource'})
                response.status_code = 403
                return response
        return decorated
    return require_permission

