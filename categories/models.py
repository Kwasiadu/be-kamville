from django.db import models
from parent_categories.models import ParentCategory


class Category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64, unique=True)
    parent = models.ForeignKey(ParentCategory, related_name='sub_categories',
                               to_field='name', on_delete=models.SET_NULL,
                               null=True)

    def __str__(self):
        return self.name
