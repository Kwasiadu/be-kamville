from categories.models import Category
from products.serializers import ProductSerializer
from classifieds.serializers import ClassifiedsSerializer
from rest_framework import serializers


class CategorySerializer(serializers.ModelSerializer):
    products = ProductSerializer(read_only=True, many=True)
    classifieds = ClassifiedsSerializer(read_only=True, many=True)

    class Meta:
        model = Category

        fields = ('id', 'name', "products", "classifieds")
