from django.urls import path
from categories import views

urlpatterns = [
    path('<int:pk>/', views.CategoryDetail.as_view()),
    path('', views.CategoryList.as_view()),
]
