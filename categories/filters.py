from categories.models import Category
from django_filters import rest_framework as filters


class CategoryFilter(filters.FilterSet):
    query = filters.Filter(field_name="name", lookup_expr='icontains')

    class Meta:
        model = Category
        fields = ('query',)

    @property
    def qs(self):
        query = self.request.query_params.get('query', '')
        return Category.objects.filter(name__icontains=query) \
            .order_by('name')
