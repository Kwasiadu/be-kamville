from rest_framework import generics
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_cookie

from categories.models import Category
from categories.serializers import CategorySerializer
from categories.filters import CategoryFilter


class CategoryList(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    filter_class = CategoryFilter

    @method_decorator(cache_page(60*60*24))
    @method_decorator(vary_on_cookie)
    def list(self, request, *args, **kwargs):
        return super(CategoryList, self).list(request, *args, **kwargs)


class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all().order_by('id')
    serializer_class = CategorySerializer

