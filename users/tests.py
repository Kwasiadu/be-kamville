from django.test import TestCase
from rest_framework.test import APIClient
from users.models import User

model = User
url = '/api/users/'
url_one = url + 'kwasispam@gmail.com/'
admin_str = '?admin=true'
owner_str = "?admin=false"
admin_url = url + admin_str
admin_url_one = url_one + admin_str
owner_url = url + owner_str
owner_url_one = url_one + owner_str

test_field = 'email'
post_data = {
    'email': 'john.smith@gmail.com',
    'first_name': 'John',
    'last_name': 'Smith',
}

create_data = [
    {'email': 'kwasispam@gmail.com'},
    {'email': 'jane.smith@gmail.com'}
]
query_data = {'query': create_data[0][test_field]}
admin_query_data = {'query': create_data[0][test_field], 'admin': 'true'}
update_data = {'email': 'adwoa.bankye@gmail.com'}


class UserTestCase(TestCase):

    def setUp(self):
        # Using rest framework's client
        self.client = APIClient()
        model.objects.create(**create_data[0])
        model.objects.create(**create_data[1])

    # Test routes
    def test_get_all(self):
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        response = self.client.get(admin_url)
        self.assertEqual(response.data[-1][test_field],
                         create_data[0][test_field])

    def test_search(self):
        response = self.client.get(url, query_data)
        self.assertEqual(response.status_code, 403)
        response = self.client.get(url, admin_query_data)
        response_data = response.data[0][test_field]
        test_data = create_data[0][test_field]
        self.assertEqual(response_data, test_data)

    def test_post(self):
        response = self.client.post(url, post_data)
        self.assertEqual(response.data[test_field], post_data[test_field])

    def test_get_one(self):
        # Test regular/unauthenticated user
        response = self.client.get(url_one)
        self.assertEqual(response.status_code, 403)
        # Test user who owns the resource
        response = self.client.get(owner_url_one)
        self.assertEqual(response.data[test_field], create_data[0][test_field])
        # Test admin user
        response = self.client.get(admin_url_one)
        self.assertEqual(response.data[test_field], create_data[0][test_field])

    def test_update(self):
        response = self.client.put(url_one, update_data)
        self.assertEqual(response.status_code, 403)
        response = self.client.put(admin_url_one, update_data)
        self.assertEqual(response.data[test_field], update_data[test_field])

    def test_delete(self):
        response = self.client.delete(url_one)
        self.assertEqual(response.status_code, 403)
        response = self.client.delete(admin_url_one)
        self.assertEqual(response.status_code, 204)
