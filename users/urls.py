from django.urls import path
from users.views import UserViewSet, UserDetail, UserDetailById

urlpatterns = [
    path('<str:email>/', UserDetail.as_view()),
    path('<str:pk>/', UserDetailById.as_view()),
    path('', UserViewSet.as_view({'get': 'list'})),
]
