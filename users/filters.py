from django_filters import rest_framework as filters
from django.db.models import Q

from users.models import User


class UserFilter(filters.FilterSet):

    class Meta:
        model = User
        fields = ()

    @property
    def qs(self):
        query = self.request.query_params.get('query', '')
        return User.objects.filter(Q(email__icontains=query) |
                                      Q(first_name__icontains=query) |
                                      Q(last_name__icontains=query))
