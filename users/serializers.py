from rest_framework import serializers
from users.models import User
from addresses.serializers import AddressSerializer


class UserSerializer(serializers.ModelSerializer):
    addresses = AddressSerializer(many=True, read_only=True)

    class Meta:
        model = User

        fields = ('id', 'user_id', 'email', 'first_name', 'last_name',
                  'join_date', 'full_name', 'nickname', 'image_name',
                  'image_url', 'city', 'image_folder', 'addresses')
        read_only_fields = ('join_date',)


