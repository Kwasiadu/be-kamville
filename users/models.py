from django.db import models

# TODO: fix amazon s3 cors issue
USER_URL = 'https://kamvillee.s3.amazonaws.com/static/img/accounts/'


# Focuses on profile/personalization and authorization
class User(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.CharField(max_length=32, unique=True, null=True)
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=32, blank=True)
    last_name = models.CharField(max_length=32, blank=True)
    nickname = models.CharField(max_length=32, blank=True)
    join_date = models.DateField(auto_now_add=True, editable=False)
    city = models.CharField(max_length=32, blank=True)
    image_name = models.CharField(max_length=64, blank=True)
    is_seller = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    def __str__(self):
        return self.email

    @property
    def image_url(self):
        return USER_URL + self.image_name

    @property
    def full_name(self):
        return ' '.join([self.first_name, self.last_name])

    @property
    def image_folder(self):
        return USER_URL


