from rest_framework import generics
import lob
import os
from django.http import JsonResponse
import json
from django.views.decorators.csrf import csrf_exempt
from rest_framework.permissions import IsAuthenticated, AllowAny


from addresses.models import Address
from addresses.serializers import AddressSerializer


class AddressList(generics.ListCreateAPIView):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer


class AddressDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Address.objects.all()
    serializer_class = AddressSerializer


@csrf_exempt
def verify_address(request):
    address = json.loads(request.body)
    lob.api_key = os.environ.get("LOB_LIVE_SK", "")
    primary_line = address.get('street_line1', ''),
    secondary_line = address.get('street_line2', ''),
    city = address.get('city', ''),
    state = address.get('state')
    if type(state) == 'dict':
        state = state.get('code')
    zip_code = address.get('zip', ''),
    country = address.get('country')
    if type(country) == 'dict':
        country = country.get('code')
    if country == 'US' or country == 'United States':
        response = lob.USVerification.create(
            primary_line=primary_line,
            secondary_line=secondary_line,
            city=city,
            state=state,
            zip_code=zip_code
            )
    else:
        response = lob.IntlVerification.create(
            primary_line=primary_line,
            secondary_line=secondary_line,
            city=city,
            country=country
        )
    return JsonResponse(response)

















