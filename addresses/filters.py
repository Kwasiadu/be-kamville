from django_filters import rest_framework as filters

from addresses.models import Address


class AddressFilter(filters.FilterSet):
    customer = filters.Filter(field_name="customer", lookup_expr='icontains')

    class Meta:
        model = Address
        fields = ('customer',)


