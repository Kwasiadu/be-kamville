from django.urls import path
from addresses.views import AddressList, AddressDetail, verify_address

urlpatterns = [
    path('verify/', verify_address),
    path('<int:pk>/', AddressDetail.as_view()),
    path('', AddressList.as_view()),
]
