from django.test import TestCase
from rest_framework.test import APIClient
from addresses.models import Address
from users.models import User
from users.tests import create_data as users_create_data


model = Address
url = '/api/addresses/'
url_one = url + '1/'
admin_str = '?admin=true'
owner_str = "?admin=false"
admin_url = url + admin_str
admin_url_one = url_one + admin_str
owner_url = url + owner_str
owner_url_one = url_one + owner_str

test_field = 'street_line1'
post_data = {
    'first_name': 'John',
    'last_name': 'Smith',
    'phone': '555-555-5555',
    'organization': 'The kamville',
    'street_line1': '1 Main st',
    'street_line2': 'Apt 1',
    'city': 'New York',
    'state': 'NY',
    'zip': '10000-1000',
    'country': 'USA',
    'verified': 'true',
    'checked': 'true',
}

create_data = [
    {'id': 1, 'first_name': 'John', 'last_name': 'Smith',
     'street_line1': '555 walker dr', 'city': 'town', 'country': 'USA'},
    {'id': 2, 'first_name': 'John', 'last_name': 'Chang',
     'street_line1': '707 continental dr', 'city': 'town', 'country': 'USA'},
]
query_data = {'query': create_data[0][test_field]}
admin_query_data = {'query': create_data[0][test_field], 'admin': 'true'}
update_data = {'id': 1, 'first_name': 'John', 'last_name': 'Smith',
               'street_line1': '750 main st', 'city': 'town', 'country': 'USA'}


class AddressTestCase(TestCase):

    def setUp(self):
        # Using rest framework's client
        self.client = APIClient()

        # Add foreign key objects
        first_donor = User.objects.create(**users_create_data[0])
        second_donor = User.objects.create(**users_create_data[1])
        post_data['email'] = second_donor.email
        create_data[0]['email'] = first_donor

        model.objects.create(**create_data[0])
        model.objects.create(**create_data[1])

    # Test routes
    def test_get_all(self):
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        response = self.client.get(admin_url)
        self.assertEqual(response.data[0][test_field],
                         create_data[0][test_field])

    def test_search(self):
        response = self.client.get(url, query_data)
        self.assertEqual(response.status_code, 403)
        response = self.client.get(url, admin_query_data)
        response_data = response.data[0][test_field]
        test_data = create_data[0][test_field]
        self.assertEqual(response_data, test_data)

    def test_post(self):
        response = self.client.post(url, post_data)
        self.assertEqual(response.data[test_field], post_data[test_field])

    def test_get_one(self):
        # Test regular/unauthenticated user
        response = self.client.get(url_one)
        self.assertEqual(response.status_code, 403)
        # Test user who owns the resource
        response = self.client.get(owner_url_one)
        self.assertEqual(response.data[test_field], create_data[0][test_field])
        # Test admin user
        response = self.client.get(admin_url_one)
        self.assertEqual(response.data[test_field], create_data[0][test_field])

    def test_update(self):
        response = self.client.put(url_one, update_data)
        self.assertEqual(response.status_code, 403)
        response = self.client.put(admin_url_one, update_data)
        self.assertEqual(response.data[test_field], update_data[test_field])

    def test_delete(self):
        response = self.client.delete(url_one)
        self.assertEqual(response.status_code, 403)
        response = self.client.delete(admin_url_one)
        self.assertEqual(response.status_code, 204)
