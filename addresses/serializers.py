from addresses.models import Address
from rest_framework import serializers


class AddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address

        fields = ('id', 'first_name', 'last_name', 'phone', 'p_o_box',
                  'street_line1', 'street_line2', 'city',
                  'country', 'verified', 'default')

