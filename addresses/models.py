from django.db import models
from users.models import User


class Address(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    phone = models.CharField(max_length=16, blank=True, null=True)
    street_line1 = models.CharField(max_length=64, blank=True, null=True)
    street_line2 = models.CharField(max_length=64, blank=True, null=True)
    p_o_box = models.CharField(max_length=16, blank=True, null=True)
    city = models.CharField(max_length=32)
    country = models.CharField(max_length=32, default="Ivory Coast")
    verified = models.BooleanField(default=False)
    default = models.BooleanField(default=False)
    user = models.ForeignKey(User, related_name='addresses', blank=True,
                             null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.street_line1 + '\n' + self.city
