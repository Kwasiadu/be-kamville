from django.urls import path
from customers.views import CustomerViewSet, CustomerDetail,\
    CustomerViewSetWithOrders, CustomerDetailViaEmail

urlpatterns = [
    path('<str:email>/', CustomerDetailViaEmail.as_view()),
    path('<str:pk>/', CustomerDetail.as_view()),
    path('<str:pk>/orders', CustomerViewSetWithOrders.as_view()),
    path('', CustomerViewSet.as_view()),
]
