from rest_framework.generics import \
    ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
import re

from customers.models import Customer
from customers.serializers import CustomerSerializer
from customers.filters import CustomerFilter

from auth0authorization.utils import requires_permission_or_owner


class CustomerViewSet(ListCreateAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    filter_class = CustomerFilter


class CustomerViewSetWithOrders(ListCreateAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    filter_class = CustomerFilter


class CustomerDetail(RetrieveUpdateDestroyAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer


class CustomerDetailViaEmail(RetrieveUpdateDestroyAPIView):
    lookup_field = 'email'
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer

    @requires_permission_or_owner('write:all', ('email',))
    def get(self, request, *args, **kwargs):
        regex = '^(\w|\.|\_|\-)+[@](\w|\_|\-|\.)+[.]\w{2,3}$'
        lookup_detail = kwargs.get('email')
        print('email', lookup_detail)
        # If user is not in our system, create a record for user
        if re.search(regex, lookup_detail):
            instance = Customer.objects.get_or_create(email=lookup_detail)[0]
            serializer = self.get_serializer(instance)
            return Response(serializer.data)
        elif lookup_detail.isdigit():
            instance = Customer.objects.get(id=lookup_detail)
            serializer = self.get_serializer(instance)
            return Response(serializer.data)
        else:
            print('invalid detail', lookup_detail)
        return Response()
