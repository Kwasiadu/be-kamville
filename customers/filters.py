from django_filters import rest_framework as filters
from customers.models import Customer


class CustomerFilter(filters.FilterSet):
    customer = filters.Filter(field_name="email", lookup_expr='icontains')

    class Meta:
        model = Customer
        fields = ('id', 'customer')
