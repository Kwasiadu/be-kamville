from rest_framework import serializers
from customers.models import Customer
from orders.serializers import OrderSerializer
from users.serializers import UserSerializer


class CustomerSerializer(serializers.ModelSerializer):
    orders = OrderSerializer(read_only=True, many=True)
    user = UserSerializer(read_only=True)

    class Meta:
        model = Customer
        fields = ('id', 'email', 'user', 'orders',)


