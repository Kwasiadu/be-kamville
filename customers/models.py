from django.db import models
from users.models import User


# Focus on purchases
class Customer(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='customer', null=True,
                             blank=True, to_field='email',
                             on_delete=models.CASCADE)
    email = models.EmailField(unique=True)

    def __str__(self):
        return self.email



