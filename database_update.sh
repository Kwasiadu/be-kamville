#!/usr/bin/env bash

prod_server=ubuntu@3.87.86.239


dbName=kamville_db
idFile=~/Documents/AWS/kamville.pem
sqlFile=kamville_prod-$(date +%F).sql
folder=db_backups
originFile=${folder}/${sqlFile}
backupFile=${folder}/kamville_${1}-$(date +%F).sql


[[ -z ${1} ]] && echo Bad input. Please pass 'local', 'dev' or 'staging' as the argument && exit 1

# create backup folder if necessary
[[ ! -d ~/${folder} ]] && mkdir ~/${folder}
# copy prod database file
ssh -i ${idFile} ${prod_server} "mysqldump -u kamville -p${KAMVILLE_DB_PASSWORD} ${dbName} > ${originFile}";
# copy data file to local backup directory
scp -i ${idFile} ${prod_server}:${originFile} ~/${folder}
wait
if [[ "$server" == "local" ]]
then
    # backup local database
    mysqldump -u kamville -p${KAMVILLE_DB_PASSWORD} ${dbName} > ~/${backupFile}
    # load data file into local database
    echo Updating $1 database with ${2} table from production server: ${prod_server}
    mysql -u kamville -p${KAMVILLE_DB_PASSWORD} ${dbName} < ~/${originFile}
else
    # create backup folder if necessary
    ssh -i ${idFile} ${server} "[[ ! -d ~/${folder} ]] && mkdir ~/${folder}"
    #copy datafile
    scp -i ${idFile} ~/${originFile} ${server}:${folder}
    # backup database
    ssh -i ${idFile} ${server} "mysqldump -u kamville -p${KAMVILLE_DB_PASSWORD} ${dbName} > ~/${backupFile}"
    # drop database
    ssh -i ${idFile} ${server} "mysqladmin -u kamville -p${KAMVILLE_DB_PASSWORD} drop --force ${dbName}"
    # recreate database
    ssh -i ${idFile} ${server} "mysqladmin -u kamville -p${KAMVILLE_DB_PASSWORD} create ${dbName}"
    # load data into recreated database
    echo Updating $1 database with data from production server: ${prod_server}
    ssh -i ${idFile} ${server} "mysql -u kamville -p${KAMVILLE_DB_PASSWORD} ${dbName} < ~/${originFile}"
fi
# remove dataFile
rm ~/${originFile}
