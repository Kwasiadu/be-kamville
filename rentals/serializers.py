from rest_framework import serializers
from rentals.models import Rental
from amenities.serializers import AmenitiesSerializer
from geolocation.serializers import GeolocationSerializer
from users.serializers import UserSerializer


class RentalSerializer(serializers.ModelSerializer):
    amenities = AmenitiesSerializer(read_only=True)
    geolocation = GeolocationSerializer(read_only=True)
    seller = UserSerializer(read_only=True)

    class Meta:
        model = Rental

        fields = ('id',  'title',  'price', 'category',
                  'seller', 'address', 'geolocation', 'amenities', 'publish',
                  'images', 'image_folder',
                  'image1',
                  'image2',
                  'image3',
                  'image4',
                  'image5',
                  )

