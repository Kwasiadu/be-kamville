from django_filters import rest_framework as filters
from rentals.models import Rental
from kamville.utils import get_user_email


class RentalFilter(filters.FilterSet):
    query = filters.Filter(field_name="title", lookup_expr='icontains')

    class Meta:
        model = Rental
        fields = ('query',)

    @property
    def qs(self):
        query = self.request.query_params.get('query', '')
        return Rental.objects.filter(title__icontains=query) \
            .order_by('title')
