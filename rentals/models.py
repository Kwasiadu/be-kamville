from django.db import models
from users.models import User
from addresses.models import Address
from amenities.models import Amenities
from geolocation.models import Geolocation

RENTALS_URL = 'https://kamvillee.s3.amazonaws.com/static/img/rentals/'


class Rental(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=64)
    price = models.DecimalField(max_digits=16, decimal_places=2)
    category = models.CharField(max_length=32)
    image1 = models.CharField(max_length=64)
    image2 = models.CharField(max_length=64, blank=True)
    image3 = models.CharField(max_length=64, blank=True)
    image4 = models.CharField(max_length=64, blank=True)
    image5 = models.CharField(max_length=64, blank=True)

    seller = models.ForeignKey(User, related_name='rentals',
                               on_delete=models.CASCADE)
    address = models.ForeignKey(Address, related_name='rentals',
                                on_delete=models.SET_NULL, blank=True, null=True)
    geolocation = models.ForeignKey(Geolocation, related_name='rentals',
                                on_delete=models.SET_NULL, blank=True, null=True)
    amenities = models.ForeignKey(Amenities, related_name='rentals',
                                on_delete=models.SET_NULL, blank=True, null=True)
    publish = models.BooleanField(verbose_name="Publish listing?", default=True)

    def __str__(self):
        return self.title

    @property
    def image_folder(self):
        return self.title.lower().replace(" ", "_") + "/"

    @property
    def images(self):
        image_list = []
        if self.image1:
            image_list.append(RENTALS_URL + self.image_folder + self.image1)
        if self.image2:
            image_list.append(RENTALS_URL + self.image_folder + self.image2)
        if self.image3:
            image_list.append(RENTALS_URL + self.image_folder + self.image3)
        if self.image4:
            image_list.append(RENTALS_URL + self.image_folder + self.image4)
        if self.image5:
            image_list.append(RENTALS_URL + self.image_folder + self.image5)
        return image_list




