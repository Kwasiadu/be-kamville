from rest_framework import generics
from kamville.utils import requires_scope
from rentals.models import Rental
from rentals.serializers import RentalSerializer
from rentals.filters import RentalFilter


class RentalList(generics.ListCreateAPIView):
    queryset = Rental.objects.all()
    serializer_class = RentalSerializer
    filter_class = RentalFilter

    # @requires_scope('write:all')
    def post(self, request, *args, **kwargs):
        return super(RentalList, self).post(request, *args, **kwargs)


class RentalDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Rental.objects.all().order_by('id')
    serializer_class = RentalSerializer

    # @requires_scope('write:all')
    def post(self, request, *args, **kwargs):
        return super(RentalDetail, self).put(request, *args, **kwargs)

    # @requires_scope('write:all')
    def put(self, request, *args, **kwargs):
        return super(RentalDetail, self).put(request, *args, **kwargs)

    # @requires_scope('write:all')
    def delete(self, request, *args, **kwargs):
        return super(RentalDetail, self).delete(request, *args, **kwargs)
