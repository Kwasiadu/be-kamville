from django.urls import path
from rentals import views

urlpatterns = [
    path('<int:pk>/', views.RentalDetail.as_view()),
    path('', views.RentalList.as_view()),
]
