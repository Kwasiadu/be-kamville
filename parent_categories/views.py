from rest_framework import generics, permissions
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_cookie

from kamville.utils import requires_scope
from parent_categories.models import ParentCategory
from parent_categories.serializers import ParentCategorySerializer
from parent_categories.filters import ParentCategoryFilter


class ParentCategoryList(generics.ListCreateAPIView):
    queryset = ParentCategory.objects.all()
    serializer_class = ParentCategorySerializer
    filter_class = ParentCategoryFilter
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


    @method_decorator(cache_page(60 * 60 * 24))
    @method_decorator(vary_on_cookie)
    def list(self, request, *args, **kwargs):
        return super(ParentCategoryList, self).list(request, *args, **kwargs)


class ParentCategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ParentCategory.objects.all().order_by('id')
    serializer_class = ParentCategorySerializer
