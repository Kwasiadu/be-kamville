from django.apps import AppConfig


class ParentCategoriesConfig(AppConfig):
    name = 'parent_categories'
