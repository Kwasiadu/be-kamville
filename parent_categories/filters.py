from parent_categories.models import ParentCategory
from django_filters import rest_framework as filters


class ParentCategoryFilter(filters.FilterSet):
    query = filters.Filter(field_name="name", lookup_expr='icontains')

    class Meta:
        model = ParentCategory
        fields = ('query',)

    @property
    def qs(self):
        query = self.request.query_params.get('query', '')
        return ParentCategory.objects.filter(name__icontains=query) \
            .order_by('name')
