from parent_categories.models import ParentCategory
from products.serializers import ProductSerializer
from classifieds.serializers import ClassifiedsSerializer
from rest_framework import serializers


class ParentCategorySerializer(serializers.ModelSerializer):
    # products = ProductSerializer(read_only=True, many=True)
    # classifieds = ClassifiedsSerializer(read_only=True, many=True)

    class Meta:
        model = ParentCategory
        fields = ('name',)

