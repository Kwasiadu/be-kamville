from django.urls import path
from geolocation import views

urlpatterns = [
    path('<int:pk>/', views.GeolocationDetail.as_view()),
    path('', views.GeolocationList.as_view()),
]
