from django.db import models


# Create your models here.
class Geolocation(models.Model):
	lat = models.FloatField()
	long = models.FloatField()

	def __str__(self):
		return "[" + str(self.lat) + ", " + str(self.long) + "]"
