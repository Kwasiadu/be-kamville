from rest_framework import generics
from kamville.utils import requires_scope
from geolocation.models import Geolocation
from geolocation.serializers import GeolocationSerializer


class GeolocationList(generics.ListCreateAPIView):
    queryset = Geolocation.objects.all()
    serializer_class = GeolocationSerializer

    # @requires_scope('write:all')
    def post(self, request, *args, **kwargs):
        return super(GeolocationList, self).post(request, *args, **kwargs)


class GeolocationDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Geolocation.objects.all().order_by('id')
    serializer_class = GeolocationSerializer

    # @requires_scope('write:all')
    def post(self, request, *args, **kwargs):
        return super(GeolocationDetail, self).put(request, *args, **kwargs)

    # @requires_scope('write:all')
    def put(self, request, *args, **kwargs):
        return super(GeolocationDetail, self).put(request, *args, **kwargs)

    # @requires_scope('write:all')
    def delete(self, request, *args, **kwargs):
        return super(GeolocationDetail, self).delete(request, *args, **kwargs)
