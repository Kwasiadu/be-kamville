from geolocation.models import Geolocation
from rest_framework import serializers


class GeolocationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Geolocation

        fields = ('id',  'lat', 'long', )
