from django.test import TestCase
from rest_framework.test import APIClient
from payments.models import Payment
from users.models import User
from users.tests import create_data as users_create_data

model = Payment
url = '/api/payments/'
url_one = url + '1/'
admin_str = '?admin=true'
owner_str = "?admin=false"
admin_url = url + admin_str
admin_url_one = url_one + admin_str
owner_url = url + owner_str
owner_url_one = url_one + owner_str

test_field = 'customer_id'
post_data = {
    'customer_id': 'sajlsjf',
    'amount': 100,
    'plan': 'One-time payment',
    'child': '',
    'rate': '',
    'card_name': '',
    'card_phone': '',
    'card_last4': '4242',
    'test_mode': 'true',
}

create_data = [
    {'id': 1, 'customer_id': 'Kwasi1',
     'amount': 100, 'card_last4': '4444'},
    {'id': 2, 'customer_id': 'Kwasi2',
     'amount': 200, 'card_last4': '1111'},
]
query_data = {'query': create_data[0][test_field]}
admin_query_data = {'query': create_data[0][test_field], 'admin': 'true'}
update_data = {'customer_id': 'Kwasi3', 'amount': 300, 'card_last4': '5555'}


class PaymentTestCase(TestCase):

    def setUp(self):
        # Using rest framework's client
        self.client = APIClient()

        # Add foreign key objects
        user = User.objects.create(**users_create_data[0])
        post_data['user'] = user.email
        post_data['user_email'] = user.email
        create_data[0]['user'] = user
        create_data[1]['user'] = user

        model.objects.create(**create_data[0])
        model.objects.create(**create_data[1])

    # Test routes
    def test_get_all(self):
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        response = self.client.get(admin_url)
        self.assertEqual(response.data[0][test_field],
                         create_data[-1][test_field])

    def test_search(self):
        response = self.client.get(url, query_data)
        self.assertEqual(response.status_code, 403)
        response = self.client.get(url, admin_query_data)
        response_data = response.data[0][test_field]
        test_data = create_data[0][test_field]
        self.assertEqual(response_data, test_data)

    def test_post(self):
        response = self.client.post(url, post_data)
        self.assertEqual(response.data[test_field], post_data[test_field])

    def test_get_one(self):
        # Test regular/unauthenticated user
        response = self.client.get(url_one)
        self.assertEqual(response.status_code, 403)
        # Test user who owns the resource
        response = self.client.get(owner_url_one)
        self.assertEqual(response.data[test_field], create_data[0][test_field])
        # Test admin user
        response = self.client.get(admin_url_one)
        self.assertEqual(response.data[test_field], create_data[0][test_field])

    def test_update(self):
        response = self.client.put(url_one, update_data)
        self.assertEqual(response.status_code, 403)
        response = self.client.put(admin_url_one, update_data)
        self.assertEqual(response.data[test_field], update_data[test_field])

    def test_delete(self):
        response = self.client.delete(url_one)
        self.assertEqual(response.status_code, 403)
        response = self.client.delete(admin_url_one)
        self.assertEqual(response.status_code, 204)
