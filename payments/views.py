from rest_framework import generics
from django.http import JsonResponse
import os
import base64, uuid, json
from urllib.parse import urlencode
from http.client import HTTPSConnection

from payments.models import Payment
from payments.serializers import PaymentSerializer
from payments.filters import PaymentFilter

import logging
from mtnmomo.collection import Collection
from mtnmomo.disbursement import Disbursement
from mtnmomo.remittance import Remittance
from mtnmomo.errors import ValidationError

# Get an instance of a logger
logger = logging.getLogger('django')


class PaymentViewSet(generics.ListCreateAPIView):
    queryset = Payment.objects.order_by('-created_at')
    serializer_class = PaymentSerializer
    filter_class = PaymentFilter


class PaymentDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PaymentSerializer
    queryset = Payment.objects.order_by('-created_at')


# def get_payment_signature(data):
#     live_key = os.environ.get("CINET_API_KEY", "")
#     test_key = os.environ.get("CINET_API_KEY_TEST", "")
#     cinet_id = os.environ.get("CINET_ID", "")
#
#     if 'get' not in data:
#         data = {
#             'test_mode': 'true',
#             'amount': 10,
#             'trans_id': 1,
#             'designation': 'test',
#             'custom': 'custom',
#         }
#     # use test key if in test_mode
#     if data.get('test_mode') == 'true':
#         api_key = test_key
#     else:
#         api_key = live_key
#
#     url = "https://api.cinetpay.com/v1/?method=getSignatureByPost"
#     options = {
#         "cpm_amount": data.get("amount"),
#         "cpm_currency": "CFA",
#         "cpm_site_id": cinet_id,
#         "cpm_trans_id": data.get("trans_id"),
#         "cpm_trans_date": datetime.now(),
#         "cpm_payment_config": "SINGLE",
#         "cpm_page_action": "PAYMENT",
#         "cpm_version": "V2",
#         "cpm_language": "en",
#         "cpm_designation": data.get("designation"),
#         "cpm_custom": data.get("custom"),
#         "apikey": api_key,
#     }
#     res = requests.post(url, data=options)
#     json = res.json()
#     print('json', json)
#     return json
#     # return JsonResponse(json)
#
#
# def send_payment_to_cinet(data):
#     if 'get' not in data:
#         data = {
#             'test_mode': 'true',
#             'amount': 10,
#             'trans_id': 9,
#             'id': 1,
#             'designation': 'test',
#             'custom': 'custom',
#             'signature': 'signature'
#
#         }
#     signature = get_payment_signature(data)
#     print('sig', signature)
#
#     site_url = "https://kamville.com"
#     live_key = os.environ.get("CINET_API_KEY", "")
#     test_key = os.environ.get("CINET_API_KEY_TEST", "")
#     cinet_id = os.environ.get("CINET_ID", "")
#     # use test key if in test_mode
#     if data.get('test_mode') == 'true':
#         api_key = test_key
#     else:
#         api_key = live_key
#
#     url = "https://secure.cinetpay.com"
#     options = {
#         "cpm_amount": data.get("amount"),
#         "cpm_currency": "CFA",
#         "cpm_site_id": cinet_id,
#         "cpm_trans_id": data.get("id"),
#         "cpm_trans_date": datetime.now(),
#         "cpm_payment_config": "SINGLE",
#         "cpm_page_action": "PAYMENT",
#         "cpm_version": "V2",
#         "cpm_language": "fr",
#         "cpm_designation": data.get("designation"),
#         "cpm_custom": data.get("custom"),
#         "apikey": api_key,
#         "signature": signature,
#         "debug": 0,
#         "notify_url": site_url + "/orderconfirmation",
#         "return_url": site_url,
#         "cancel_url": site_url + "/cancelorder",
#     }
#     res = requests.post(url, data=options)
#     print('reason', res.reason)
#     json = res.json()
#     print('json', json)
#     return JsonResponse(json)
#
#
# def getTransactionHistory(data):
#     if 'get' not in data:
#         data = {
#             'test_mode': 'true',
#             'amount': 10,
#             'trans_id': 9,
#             'id': 1,
#             'designation': 'test',
#             'custom': 'custom',
#             'signature': 'signature'
#         }
#     live_key = os.environ.get("CINET_API_KEY", "")
#     test_key = os.environ.get("CINET_API_KEY_TEST", "")
#     cinet_id = os.environ.get("CINET_ID", "")
#     # use test key if in test_mode
#     if data.get('test_mode') == 'true':
#         api_key = test_key
#     else:
#         api_key = live_key
#
#     options = {
#         "cpm_site_id": cinet_id,
#         "date_debut": "01/01/2020",
#         "date_fin": datetime.now(),
#         "cpm_payid": "designation",
#         "cel_phone_num": "",
#         "cpm_trans_id": data.get("id"),
#         "apikey": api_key,
#     }
#     url = "https://api.cinetpay.com/v1/?method=getTransHistory",
#     res = requests.post(url, data=options)
#     print('reason', res.reason)
#     json = res.json()
#     print('json', json)
#     return JsonResponse(json)


def make_mtn_payment(data):
    client = Collection({
        "BASE_URL": os.environ.get("BASE_URL", "https://sandbox.momodeveloper.mtn.com"),
        "CALLBACK_HOST": os.environ.get("CALLBACK_HOST", "http://127.0.0.1:8000/"),
        "COLLECTION_USER_ID": os.environ.get("COLLECTION_USER_ID", 'e7b11109-58d6-4138-9f64-8e3ea6e9e792'),
        "COLLECTION_API_SECRET": os.environ.get("COLLECTION_API_SECRET", 'b507fe698d9a4791a7c290fb4aaf3a7b'),
        "COLLECTION_PRIMARY_KEY": os.environ.get("COLLECTION_PRIMARY_KEY", "44b321d4d9484d4f89f78b29615eac37"),
    })
    # data = {
    #     "providerCallbackHost": "http://127.0.0.1:8000"
    # }
    # auth_headers = {
    #     # 'X-Target-Environment': 'sandbox',
    #     'Content-Type': 'application/json',
    #     # 'Host': 'sandbox.momodeveloper.mtn.com',
    #     'X-Reference-Id': 'd98c6a9f-a323-472e-886a-d4720dae60d0',
    #     'X-Request-Verification-Token': 'ExCV0tX5n-IUopMuWiCl-K8M_c9tHK43Z-HBhe9zDtQqMrEh7ikiX0XtztfDhWZBIHDtSIKUAyaR7a70F36iEgow8ksA3dlBcSFnW36ZKdUM6CbcMBbXQhT3JA-wEp9evevmcVreijR6OnWugN7jjA2',
    #     'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36',
    #     'Origin': 'http://127.0.0.1:8000',
    #     'Referer': 'http://127.0.0.1:8000/api/payments'
    # }
    #
    # user = requests.post("https://sandbox.momodeveloper.mtn.com/v1_0/apiuser", headers=auth_headers, data=data)
    # print('user', user)
    # print('reason', user.status_code, user.reason)
    #
    #



    # url = "https://sandbox.momodeveloper.mtn.com/collection/v1_0/requesttopay"
    # data = {
    #     'payer': {
    #         'partyIdType': 'MSISDN',
    #         'partyId': '256772123456'
    #     },
    #     'payeeNote': 'dd',
    #     'payerMessage': 'testing my computer program',
    #     'externalId': '123456789',
    #     'currency': 'EUR',
    #     'amount': '6'
    # }
    #
    # headers = {'X-Target-Environment': 'sandbox', 'Content-Type': 'application/json',
    #  'X-Reference-Id': 'ee544bee-1312-451c-8ad9-c570f6dfaecf',
    #  'Ocp-Apim-Subscription-Key': '44b321d4d9484d4f89f78b29615eac37'}
    # manual_response = requests.post(url, headers=headers, data=json.dumps(data))
    # print('manual response', manual_response)

    try:
        first_response = client.requestToPay(
            mobile="22566532514", amount="6", external_id="123456789",
            payee_note="dd", payer_message="testing my computer program", currency="CFA")
        print('first response succeeded', first_response)
    except ValidationError as e:
        print('first response failed', e)

    try:
        second_response = client.requestToPay(
            mobile="22505100629", amount="6", external_id="123456789",
            payee_note="dd", payer_message="testing my computer program", currency="CFA")
        print('second response succeeded', second_response)
    except ValidationError as e:
        print('second response failed', e)

    try:
        failed_response = client.requestToPay(
            mobile="46733123450", amount="6", external_id="123456789",
            payee_note="dd", payer_message="testing my computer program", currency="EUR")
        print('failed response succeeded', failed_response)
    except ValidationError as e:
        print('failed response failed', e)

    try:
        success_response = client.requestToPay(
            mobile="256772123456", amount="6", external_id="123456789",
            payee_note="dd", payer_message="testing my computer program", currency="EUR")
        print('success response succeeded', success_response)
    except ValidationError as e:
        print('success response failed', e)

    try:
        rejected_response = client.requestToPay(
            mobile="46733123451", amount="6", external_id="123456789",
            payee_note="dd", payer_message="testing my computer program", currency="EUR")
        print('rejected response succeeded', rejected_response)
    except ValidationError as e:
        print('rejected response failed', e)

    try:
        timeout_response = client.requestToPay(
            mobile="46733123452", amount="6", external_id="123456789",
            payee_note="dd", payer_message="testing my computer program", currency="EUR")
        print('timeout response succeeded', timeout_response)
    except ValidationError as e:
        print('timeout response failed', e)

    try:
        ongoing_response = client.requestToPay(
            mobile="46733123453", amount="6", external_id="123456789",
            payee_note="dd", payer_message="testing my computer program", currency="EUR")
        print('ongoing response succeeded', ongoing_response)
    except ValidationError as e:
        print('ongoing response failed', e)

    try:
        pending_response = client.requestToPay(
            mobile="46733123454", amount="6", external_id="123456789",
            payee_note="dd", payer_message="testing my computer program", currency="EUR")
        print('pending response succeeded', pending_response)
    except ValidationError as e:
        print('pending response failed', e)

    try:
        response = first_response
    except Exception as e:
        print('exception', e)
        response = {}
    return JsonResponse(response)


def send_mtn_payment_to_seller(data):
    client = Disbursement({
        "BASE_URL": os.environ.get("BASE_URL", "https://sandbox.momodeveloper.mtn.com"),
        "CALLBACK_HOST": os.environ.get("CALLBACK_HOST", "http://127.0.0.1:8000/"),
        "DISBURSEMENT_USER_ID": os.environ.get("DISBURSEMENT_USER_ID", 'eb7d92fd-1730-4de8-91af-9cb6cb5fd55c'),
        "DISBURSEMENT_API_SECRET": os.environ.get("DISBURSEMENT_API_SECRET", '2745024aa41e4813ba706859a5507ff4'),
        "DISBURSEMENT_PRIMARY_KEY": os.environ.get("DISBURSEMENT_PRIMARY_KEY", 'e781ce8be8ee403ab2803c5249056517')
    })
    response = client.transfer(amount="600",
                               mobile="256772123456",
                               external_id="123456789",
                               payee_note="dd",
                               payer_message="dd",
                               currency="EUR")
    return JsonResponse(response)


def get_balance(data):
    client = Collection({
        "BASE_URL": os.environ.get("BASE_URL", "https://sandbox.momodeveloper.mtn.com"),
        "CALLBACK_HOST": os.environ.get("CALLBACK_HOST", "http://127.0.0.1:8000/"),
        "COLLECTION_USER_ID": os.environ.get("COLLECTION_USER_ID", 'e7b11109-58d6-4138-9f64-8e3ea6e9e792'),
        "COLLECTION_API_SECRET": os.environ.get("COLLECTION_API_SECRET", 'b507fe698d9a4791a7c290fb4aaf3a7b'),
        "COLLECTION_PRIMARY_KEY": os.environ.get("COLLECTION_PRIMARY_KEY", "44b321d4d9484d4f89f78b29615eac37"),
    })
    response = client.getBalance()
    return JsonResponse(response)


def is_payer_active(data):

    client = Remittance({
        "BASE_URL": os.environ.get("BASE_URL",
                                   "https://sandbox.momodeveloper.mtn.com"),
        "CALLBACK_HOST": os.environ.get("CALLBACK_HOST",
                                        "http://127.0.0.1:8000/"),
        "REMITTANCE_USER_ID": os.environ.get("REMITTANCE_USER_ID", '1ad246c5-2b37-4812-8ac6-3b86e79e761d'),
        "REMITTANCE_API_SECRET": os.environ.get("REMITTANCE_API_SECRET", 'bdc2d93e5ef1423c9c050f54e749a0b0'),
        "REMITTANCE_PRIMARY_KEY": os.environ.get("REMITTANCE_PRIMARY_KEY", "3df9ced3cda54755ae0cd01d2a8b7579"),
    })
    response = client.isActive("22566532514")
    print('is payer active', response)
    return JsonResponse(response)


def get_api_user(data):
    reference_id = str(uuid.uuid4())

    headers = {
        "X-Reference-Id": reference_id,
        "Content-Type": 'application/json',
        'Ocp-Apim-Subscription-Key': "44b321d4d9484d4f89f78b29615eac37",
    }
    params = urlencode({})

    body = json.dumps({
        "providerCallbackHost": "http://127.0.0.1:8000/"
    })

    try:
        conn = HTTPSConnection('ericssonbasicapi2.azure-api.net')
        print('u url', "/v1_0/apiuser?{0}".format(params))
        conn.request("POST", "/v1_0/apiuser?%s" % params, body, headers)
        response = conn.getresponse()
        print('res', dir(response))
        print('status', response.status)
        print('reason', response.reason)
        data = response.read()
        print('data b', data, type(data))
        data = data.decode("utf-8")
        print('data u', data, type(data))
        conn.close()
        print('user conn closed', data)
        # return JsonResponse({})
        return reference_id, data
    except Exception as e:
        try:
            print("[Errno {0}] {1}".format(e.errno, e.strerror))
        except:
            print('err', e)


def get_api_key():
    headers = {
        "Content-Type": "application/json",
        "Ocp-Apim-Subscription-Key": "44b321d4d9484d4f89f78b29615eac37",
    }

    params = urlencode({

    })

    body = json.dumps({
        "providerCallbackHost": "http://127.0.0.1:8000/"
    })
    reference_id, user = get_api_user("")

    try:
        conn = HTTPSConnection('ericssonbasicapi2.azure-api.net')
        conn.request("POST", "/v1_0/apiuser/{0}/apikey?{1}"
                     .format(reference_id, params), body, headers)
        print('url', "/v1_0/apiuser/{0}/apikey?{1}".format(reference_id, params))
        response = conn.getresponse()
        print(response.status)
        data = response.read()
        print('data', data, type(data))
        print("_ " * 20)

        # convert bytes to string
        my_json_str = data.decode("utf-8")

        # Load the json string to a Python list &  dump it back out as formatted JSON
        my_list = json.loads(my_json_str)
        conn.close()
        print('key conn closed')
        return user, my_list.get('apiKey', None),

    except:
        pass


def manual_payment(data):

    api_user, api_key = get_api_key()

    if api_key:
        api_user_and_key = api_user + ":" + api_key
    else:
        print('u', api_key)
        print("___" * 20)
        print('k', api_user)
        return
    api_user_and_key_bytes = api_user_and_key.encode("utf-8")
    print('aukb', api_user_and_key_bytes)
    encoded = base64.b64encode(api_user_and_key_bytes)
    print('enc', encoded)
    decoded = encoded.decode('utf-8')
    print('dec', decoded)
    headers = {
        # Request headers
        "Authorization": "Basic" + decoded,
        'Ocp-Apim-Subscription-Key': "44b321d4d9484d4f89f78b29615eac37",
    }

    params = urlencode({})
    body = json.dumps({
        "providerCallbackHost": "http://127.0.0.1:8000/"
    })

    try:
        print('make conn')
        conn = HTTPSConnection('ericssonbasicapi2.azure-api.net', timeout=5)
        print('conn made', "/collection/token/?%s" % params)
        conn.request("POST", "/collection/token/?%s" % params, body, headers)
        print('request made')
        response = conn.getresponse()
        print('get response')
        print(response.status)
        data = response.read()
        print('data', data, type(data))
        print("_ " * 20)
        # convert bytes to string
        my_json_str = data.decode("utf-8")
        print('mjs', my_json_str)
        # Load the json string to a Python list &  dump it back out as formatted JSON
        my_list = json.loads(my_json_str)
        print('json', my_list)
        conn.close()
        return JsonResponse({'token': my_list.get('token', None)})
    except Exception as e:
        print('err', e)
        return JsonResponse({'err', e})


