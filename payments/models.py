from django.db import models
from orderitems.models import OrderItem

PAYMENT_STATUS_CHOICES = (
    ("Paid to User", "Paid to User"),
    ("To be Paid", "To be Paid"),

)


class Payment(models.Model):

    id = models.AutoField(primary_key=True)
    order_item = models.ForeignKey(OrderItem, on_delete=models.CASCADE)
    transaction_type = models.CharField(max_length=64, blank=True)
    disbursed = models.CharField(max_length=16,
                                 choices=PAYMENT_STATUS_CHOICES,
                                 default="To be Paid")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.id
