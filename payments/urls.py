from django.urls import path
from payments.views import PaymentViewSet, PaymentDetail, \
    make_mtn_payment, send_mtn_payment_to_seller, get_balance, \
    is_payer_active, manual_payment, get_api_user

urlpatterns = [
    path('make_mtn_payment/', make_mtn_payment, name='mtn_payment'),
    path('send_mtn_payment_to_seller/', send_mtn_payment_to_seller, name='send_payment'),
    path('get_balance/', get_balance, name='get_balance'),
    path('is_payer_active/', is_payer_active, name='is_payer_active'),
    path('manual_payment/', manual_payment, name='manual_payment'),
    path('get_user/', get_api_user, name="get api user"),
    path('<int:pk>/', PaymentDetail.as_view()),
    path('', PaymentViewSet.as_view()),
]
