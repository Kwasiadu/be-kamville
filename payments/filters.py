from django_filters import rest_framework as filters
from payments.models import Payment


class PaymentFilter(filters.FilterSet):
    query = filters.Filter(field_name="customer_id", lookup_expr='icontains')

    class Meta:
        model = Payment
        fields = ('query',)
