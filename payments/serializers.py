from rest_framework import serializers
from payments.models import Payment
from users.serializers import UserSerializer


class PaymentSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Payment

        fields = ('id', 'user', 'order_item', 'transaction_type', 'disbursed')
        read_only_fields = ('created_at', 'updated_at')
