from django.urls import path
from classifieds import views

urlpatterns = [
    path('recommended', views.RecommendedClassifiedsList.as_view()),
    path('myclassifieds', views.MyClassifiedsList.as_view()),
    path('<int:pk>/', views.ClassifiedsDetail.as_view()),
    path('', views.ClassifiedsList.as_view()),
]
