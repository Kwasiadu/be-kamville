from classifieds.models import Classifieds
from rest_framework import serializers
from users.models import User
from users.serializers import UserSerializer


class ClassifiedsSerializer(serializers.ModelSerializer):
    seller = UserSerializer(read_only=True)
    seller_id = serializers.SlugRelatedField(
        queryset=User.objects.all(), source='seller', write_only=True,
        slug_field='email'
    )

    class Meta:
        model = Classifieds
        read_only_fields = ('images', 'image_folder')

        fields = ('id',  'name',  'short_name', 'price', 'category', 'description',
                  'seller', 'seller_id', 'publish', 'images', 'image_folder',
                  'image1', 'image2', 'image3', 'image4',
                  'image5', 'video_id', 'video_url',
                  )
