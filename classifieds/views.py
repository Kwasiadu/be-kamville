from rest_framework import generics, permissions
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import AllowAny


from kamville.utils import is_owner_or_has_required_scope
from classifieds.models import Classifieds
from classifieds.serializers import ClassifiedsSerializer
from classifieds.filters import ClassifiedsFilter, \
    RecommendedClassifiedsFilter, MyClassifiedsFilter


@api_view(["GET"])
@permission_classes([AllowAny])
def list_classifieds(request):
    classifieds = Classifieds.objects.all()
    data = ClassifiedsSerializer(classifieds, many=True).data
    return JsonResponse({"results": data})


# TODO: replace authentication permissions with custom ones
class ClassifiedsList(generics.ListCreateAPIView):
    queryset = Classifieds.objects.all()
    serializer_class = ClassifiedsSerializer
    filter_class = ClassifiedsFilter
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    @method_decorator(cache_page(60*60*2))
    def list(self, request, *args, **kwargs):
        return super(ClassifiedsList, self).list(request, args, kwargs)

    def post(self, request, *args, **kwargs):
        return super(ClassifiedsList, self).post(request, *args, **kwargs)


class ClassifiedsDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Classifieds.objects.all().order_by('id')
    serializer_class = ClassifiedsSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    @method_decorator(cache_page(60*60*2))
    def get(self, request, *args, **kwargs):
        return super(ClassifiedsDetail, self).get(request, *args, **kwargs)

    # @is_owner_or_has_required_scope(['seller', 'email'], 'write:all')
    def put(self, request, *args, **kwargs):
        return super(ClassifiedsDetail, self).put(request, *args, **kwargs)

    # @is_owner_or_has_required_scope(['seller', 'email'], 'write:all')
    def delete(self, request, *args, **kwargs):
        return super(ClassifiedsDetail, self).delete(request, args, kwargs)
    
    
class MyClassifiedsList(generics.ListAPIView):
    queryset = Classifieds.objects.all()
    serializer_class = ClassifiedsSerializer
    filter_class = MyClassifiedsFilter

    def list(self, request, *args, **kwargs):
        return super().list(request, args, kwargs)


class RecommendedClassifiedsList(generics.ListAPIView):
    queryset = Classifieds.objects.all()
    serializer_class = ClassifiedsSerializer
    filter_class = RecommendedClassifiedsFilter
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def list(self, request, *args, **kwargs):
        return super(RecommendedClassifiedsList, self).list(request, args,
                                                            kwargs)


