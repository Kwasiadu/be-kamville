from django_filters import rest_framework as filters
from classifieds.models import Classifieds


class ClassifiedsFilter(filters.FilterSet):
    name = filters.Filter(field_name="name", lookup_expr='icontains')
    category = filters.Filter(field_name="category", lookup_expr='icontains')
    seller = filters.Filter(field_name='seller', lookup_expr='icontains')

    class Meta:
        model = Classifieds
        fields = ('name', 'category', 'seller')

    @property
    def qs(self):
        name = self.request.query_params.get('name', '')
        seller = self.request.query_params.get('seller', '')
        order_by = self.request.query_params.get('order_by', '')
        order_by = 'name' if order_by == "" else order_by
        category = self.request.query_params.get('category', '')
        category = "" if category == "All" else category

        return Classifieds.objects.filter(name__icontains=name,
                                          seller__email__icontains=seller,
                                          category__parent__name__icontains=category) \
            .order_by(order_by)
    
    
class MyClassifiedsFilter(filters.FilterSet):
    category = filters.Filter(field_name="category", lookup_expr='icontains')
    seller_email = filters.Filter(field_name='seller__email',
                             lookup_expr='icontains')

    class Meta:
        model = Classifieds
        fields = ('category', 'seller_email')

    @property
    def qs(self):
        seller = self.request.query_params.get('seller', '')
        category = self.request.query_params.get('category', '')
        category = "" if category == "All" else category

        return Classifieds.objects.filter(seller__email__icontains=seller,
                                          category__parent__name__icontains=category)


class RecommendedClassifiedsFilter(filters.FilterSet):
    category = filters.Filter(field_name="category", lookup_expr='icontains')

    class Meta:
        model = Classifieds
        fields = ('category',)

    @property
    def qs(self):
        category = self.request.query_params.get('category', '')
        category = "" if category == "All" else category
        return Classifieds.objects \
            .filter(category__name__icontains=category) \
            .order_by('id')[:4]
#
