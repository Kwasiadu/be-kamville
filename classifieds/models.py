from django.db import models
from users.models import User
from addresses.models import Address
from categories.models import Category

# CLASSIFIEDS_URL = 'https://kamvillee.s3.amazonaws.com/static/img/classifieds/'
CLASSIFIEDS_URL = '/static/img/classifieds/'


class Classifieds(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=128)
    short_name = models.CharField(max_length=64)
    price = models.DecimalField(max_digits=16, decimal_places=2)
    description = models.TextField()
    image1 = models.CharField(max_length=128, null=True)
    image2 = models.CharField(max_length=128, null=True, blank=True)
    image3 = models.CharField(max_length=128, null=True, blank=True)
    image4 = models.CharField(max_length=128, null=True, blank=True)
    image5 = models.CharField(max_length=128, null=True, blank=True)
    video_id = models.CharField(max_length=128, blank=True, null=True)
    category = models.ForeignKey(Category, related_name='classifieds',
                                 to_field='name', on_delete=models.PROTECT)
    seller = models.ForeignKey(User, related_name='classifieds',
                               to_field='email', on_delete=models.CASCADE)
    address = models.ForeignKey(Address, related_name='classifieds',
                                on_delete=models.SET_NULL, blank=True, null=True)
    publish = models.BooleanField(verbose_name="Publish classifieds?", default=False)

    def __str__(self):
        return self.name

    @property
    def image_folder(self):
        return self.name[:10].lower().replace(" ", "_") + "/"

    @property
    def images(self):
        image_list = []
        if self.image1:
            image_list.append(CLASSIFIEDS_URL +
                              self.image_folder + str(self.image1))
        if self.image2:
            image_list.append(CLASSIFIEDS_URL
                              + self.image_folder + str(self.image2))
        if self.image3:
            image_list.append(CLASSIFIEDS_URL + self.image_folder
                              + str(self.image3))
        if self.image4:
            image_list.append(CLASSIFIEDS_URL + self.image_folder
                              + str(self.image4))
        if self.image5:
            image_list.append(CLASSIFIEDS_URL + self.image_folder
                              + str(self.image5))
        return image_list

    @property
    def video_url(self):
        if self.video_id:
            return "https://www.youtube.com/embed/" + self.video_id + "?autoplay=1"




