from filters.models import Filter
from rest_framework import serializers


class FilterSerializer(serializers.ModelSerializer):

    class Meta:
        model = Filter

        fields = ('id', 'title', 'filters',
                  'filter1',
                  'filter2',
                  'filter3',
                  'filter4',
                  )
