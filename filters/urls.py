from django.urls import path
from filters import views

urlpatterns = [
    path('<int:pk>/', views.FilterDetail.as_view()),
    path('', views.FilterList.as_view()),
]
