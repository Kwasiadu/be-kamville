from django_filters import rest_framework as filters
from filters.models import Filter
from kamville.utils import get_user_email


class FilterFilter(filters.FilterSet):
    query = filters.Filter(field_name="title", lookup_expr='icontains')

    class Meta:
        model = Filter
        fields = ('query',)

    @property
    def qs(self):
        query = self.request.query_params.get('query', '')
        return Filter.objects.filter(title__icontains=query) \
            .order_by('title')
