from django.db import models


class Filter(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=64)
    filter1 = models.CharField(max_length=64)
    filter2 = models.CharField(max_length=64, blank=True, null=True)
    filter3 = models.CharField(max_length=64, blank=True, null=True)
    filter4 = models.CharField(max_length=64, blank=True, null=True)

    def __str__(self):
        return self.title
    
    @property
    def filters(self):
        filter_list = []
        if self.filter1:
            filter_list.append(self.filter1)
        if self.filter2:
            filter_list.append(self.filter2)
        if self.filter3:
            filter_list.append(self.filter3)
        if self.filter4:
            filter_list.append(self.filter4)
        return filter_list





