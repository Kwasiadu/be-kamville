from django.test import TestCase
from rest_framework.test import APIClient
from filters.models import Filter
from kamville.utils import get_first
from users.models import User
from users.tests import create_data as users_create_data


model = Filter
url = '/api/filter/'
url_one = url + '1/'
admin_str = '?admin=true'
admin_url = url + admin_str
admin_url_one = url_one + admin_str

test_field = 'title'
post_data = {
        'title': 'iphone 11',
        'price': 10.00,
        'category': 'phone',
        'keywords': 'phones cellphones cell, smartphone',
        'image1_name': 'iphone11.jpg',
        'image2_name': 'iphone11side.jpg',
        'publish': False,
    }

create_data = [
    {'id': 1, 'title': 'iphone 10', 'price': 10.00, 'category': 'phone', 
     'image1_name': 'iphone11.jpg', 'publish': True},
    {'id': 2, 'title': 'iphone 9', 'price': 10.00, 'category': 'phone', 
     'image1_name': 'iphone11.jpg', 'publish': True},
]
query_data = {'query': create_data[0][test_field]}
update_data = {'id': 1, 'title': 'Pixel'}


class FilterTestCase(TestCase):

    def setUp(self):
        # Using rest framework's client
        self.client = APIClient()

        # Add foreign key objects
        seller = User.objects.create(**users_create_data[0])
        post_data['seller'] = seller.email
        create_data[0]['seller'] = seller
        create_data[1]['seller'] = seller
        model.objects.create(**create_data[0])
        model.objects.create(**create_data[1])

    # Test routes
    def test_get_all(self):
        response_data = get_first(self.client.get(url), test_field)
        test_data = create_data[0][test_field]
        self.assertEqual(response_data, test_data)

    def test_search(self):
        response = self.client.get(url, query_data)
        response_data = response.data[0][test_field]
        test_data = create_data[0][test_field]
        self.assertEqual(response_data, test_data)

    def test_post(self):
        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 403)
        response = self.client.post(admin_url, post_data)
        self.assertEqual(response.data[test_field], post_data[test_field])

    def test_get_one(self):
        response = self.client.get(url_one)
        response_data = response.data[test_field]
        test_data = create_data[0][test_field]
        self.assertEqual(response_data, test_data)

    def test_update(self):
        response = self.client.put(url_one, update_data)
        self.assertEqual(response.status_code, 403)
        response = self.client.put(admin_url_one, update_data)
        self.assertEqual(response.data[test_field], update_data[test_field])

    def test_delete(self):
        response = self.client.delete(url_one)
        self.assertEqual(response.status_code, 403)
        response = self.client.delete(admin_url_one)
        self.assertEqual(response.status_code, 204)
