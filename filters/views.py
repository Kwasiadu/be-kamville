from rest_framework import generics
from kamville.utils import requires_scope
from filters.models import Filter
from filters.serializers import FilterSerializer
from filters.filters import FilterFilter


class FilterList(generics.ListCreateAPIView):
    queryset = Filter.objects.all()
    serializer_class = FilterSerializer
    filter_class = FilterFilter

class FilterDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Filter.objects.all().order_by('id')
    serializer_class = FilterSerializer

