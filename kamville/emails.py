import re
from django.core.mail import EmailMultiAlternatives
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

import logging

# Get an instance of a logger
logger = logging.getLogger('django')


class CompanyEmail:

    from_email = 'kamvillewins@gmail.com'
    to_email = 'kwasi.adu@gmail.com'
    subject = 'Hi'
    html_content = ""

    def remove_html_tags(self, content):
        """Remove html tags from a string"""
        reg_expr = re.compile('<.*?>')
        return re.sub(reg_expr, '', content)

    def __init__(self, from_email=from_email,
                 to_email=to_email, subject=subject, html_content=html_content):

        self.from_email = from_email
        self.to_email = to_email
        self.subject = subject
        self.html_content = html_content
        self.text_content = self.remove_html_tags(html_content)

    def send(self):
        msg = EmailMultiAlternatives(self.subject, self.text_content,
                                     self.from_email, [self.to_email])
        msg.attach_alternative(self.html_content, "text/html")


@csrf_exempt
def kamville_mail(request):
    import os
    from sendgrid import SendGridAPIClient
    from sendgrid.helpers.mail import Mail
    product = request.data.get('product', "Glass cleaner")
    recipient = request.data.get('user_email', 'kwasi.adu@gmail.com')
    message = Mail(
        from_email='kamvillewins@gmail.com',
        to_emails=recipient,
        subject='Your order has been placed!',
        html_content='<p>Your order for {0} has been placed</p>'.format(product)
    )
    try:
        sg = SendGridAPIClient(os.environ.get('KAMVILLE_SEND_GRID_API_KEY',
                                              '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'))
        response = sg.send(message)
        return HttpResponse(response)

    except Exception as e:
        print(e)
