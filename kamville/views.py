from rest_framework.decorators import api_view
from django.http import JsonResponse
from kamville.utils import requires_scope


@api_view(['GET'])
@requires_scope('write:all')
def private_scoped(request):
    return JsonResponse({"message": "Hello from a private endpoint!"
                                    " You need to be authenticated and "
                                    "have a scope of write:all to see this."})


def public(request):
    return JsonResponse({'message': 'Hello from a public endpoint! You don\'t need to be authenticated to see this.'})


@api_view(['GET'])
def private(request):
    return JsonResponse({'message': 'Hello from a private endpoint! You need to be authenticated to see this.'})
