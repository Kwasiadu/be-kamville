# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import json
import textwrap
from six.moves.urllib import request
import boto3
from corsheaders.defaults import default_headers

from cryptography.x509 import load_pem_x509_certificate
from cryptography.hazmat.backends import default_backend

# Get parameters from aws
try:
	ssm = boto3.client('ssm')
	SK = ssm.get_parameter(Name='KAMVILLE_DJANGO_SECRET')['Parameter']['Value']
	DB = ssm.get_parameter(Name='KAMVILLE_DB_PASSWORD')['Parameter']['Value']
	SBNA = ssm.get_parameter(Name='STORAGE_BUCKET_NAME_AWS')['Parameter'][
		'Value']
	BAAI = ssm.get_parameter(Name='KAMVILLE_AUTH0_API_ID')['Parameter']['Value']
	BAD = ssm.get_parameter(Name='KAMVILLE_AUTH0_DOMAIN')['Parameter']['Value']
except:
	pass

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(
	__file__))))

# Allow all host headers
ALLOWED_HOSTS = ['*']

DEBUG = False

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get("KAMVILLE_DJANGO_SECRET",
							"kamg$ca7x4lapp$^odi2n0z0-umez(#^2nnv^nme$h(lz+h5vcub!kam")

# from django.contrib.auth.middleware import RemoteUserMiddleware

# Application definition

INSTALLED_APPS = (
	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	'compressor',
	'corsheaders',
	'rest_framework',
	'django_filters',
	'stripe',
	'kamville',
	'users',
	'sellers',
	'customers',
	'addresses',
	'amenities',
	'parent_categories',
	'categories',
	'classifieds',
	'filters',
	'geolocation',
	'orders',
	'orderitems',
	'payments',
	'products',
	'rentals',
	'reviews',
)

#######
# AWS #
#######

KAMVILLE_AWS_STATIC_BUCKET = os.environ.get("KAMVILLE_AWS_STATIC_BUCKET", "")
KAMVILLE_AWS_ID = os.environ.get("KAMVILLE_AWS_ID", "")
KAMVILLE_AWS_SK = os.environ.get("KAMVILLE_AWS_SK", "")
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % KAMVILLE_AWS_STATIC_BUCKET

MIDDLEWARE = (
	'django.middleware.gzip.GZipMiddleware',
	'django.middleware.security.SecurityMiddleware',
	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'corsheaders.middleware.CorsMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.auth.middleware.RemoteUserMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'kamville.urls'

TEMPLATES = [
	{
		'BACKEND': 'django.template.backends.django.DjangoTemplates',
		'DIRS': [os.path.join(BASE_DIR, 'templates'), ],
		'APP_DIRS': True,
		'OPTIONS': {
			'context_processors': [
				'django.template.context_processors.debug',
				'django.template.context_processors.request',
				'django.contrib.auth.context_processors.auth',
				'django.contrib.messages.context_processors.messages',
			],
		},
	},
]

WSGI_APPLICATION = 'kamville.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = False

USE_L10N = False

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

STATICFILES_FINDERS = (
	'django.contrib.staticfiles.finders.FileSystemFinder',
	'django.contrib.staticfiles.finders.AppDirectoriesFinder',
	'compressor.finders.CompressorFinder',
)

REST_FRAMEWORK = {
	'DEFAULT_PERMISSION_CLASSES': (
		# 'rest_framework.permissions.AllowAny',
		'rest_framework.permissions.IsAuthenticated',
	),

	'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
	'PAGE_SIZE': 10,

	'DEFAULT_FILTER_BACKENDS': (
		'django_filters.rest_framework.DjangoFilterBackend',
	),

	'DEFAULT_AUTHENTICATION_CLASSES': (
		'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
		'rest_framework.authentication.SessionAuthentication',
		'rest_framework.authentication.BasicAuthentication',
	),
	# 'DEFAULT_THROTTLE_CLASSES': (
	#     'rest_framework.throttling.AnonRateThrottle',
	#     'rest_framework.throttling.UserRateThrottle'
	# ),
	# 'DEFAULT_THROTTLE_RATES': {
	#     'anon': '1000/day',
	#     'user': '10000/day'
	# }
}

# Auth0 drf setup info taken from websites below
# https://auth0.com/docs/quickstart/backend/django
# https://github.com/auth0/rules

# Download the JWKS for the Auth0 domain
# and create a public key variable with it:
try:
	jsonurl = request.urlopen(
		"https://kwasiadu.auth0.com/.well-known/jwks.json")
	jwks = json.loads(jsonurl.read())
	wrapped_base64_text = textwrap.wrap(jwks['keys'][0]['x5c'][0], 64)
	spaced_text = '\n'.join(wrapped_base64_text)
	cert = '\n'.join(('-----BEGIN CERTIFICATE-----', spaced_text,
					  '-----END CERTIFICATE-----'))

	certificate = load_pem_x509_certificate(cert.encode('utf-8'),
											default_backend())
	publickey = certificate.public_key()

	# Configure the Django REST Framework JWK by setting the JWT_AUTH variable.

	# Set the JWT_AUDIENCE to your API identifier
	#  and the JWT_ISSUER to your Auth0 domain.
	# By default those values will be retrieved from the .env file.
	JWT_AUTH = {
		'JWT_PAYLOAD_GET_USERNAME_HANDLER':
			'auth0authorization.utils.jwt_get_username_from_payload_handler',
		'JWT_DECODE_HANDLER':
			'auth0authorization.utils.jwt_decode_token',
		# 'JWT_ALGORITHM': 'RS256',
		'JWT_ALGORITHM': 'HS256',
		'JWT_AUDIENCE': os.environ.get("KAMVILLE_AUTH0_API_ID"),
		'JWT_ISSUER': os.environ.get("KAMVILLE_AUTH0_DOMAIN"),
		'JWT_AUTH_HEADER_PREFIX': 'Bearer',
	}

except:
	pass

# Add sites that should be need to have special access to the server
CORS_ORIGIN_ALLOW_ALL = True

CORS_ALLOW_CREDENTIALS = True

CORS_ALLOW_HEADERS = default_headers + (
	'auth0',
)
# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.mysql',
		'NAME': 'kamville_db',
		'USER': 'kamville',
		'PASSWORD': os.environ.get("KAMVILLE_DB_PASSWORD", "Jumiamustdie2019"),
		'OPTIONS': {
			'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
		},
	}
}

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

AUTHENTICATION_BACKENDS = [
	'django.contrib.auth.backends.ModelBackend',
	'django.contrib.auth.backends.RemoteUserBackend',
]

# MemCached

CACHES = {
	'default': {
		'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
		'LOCATION': '127.0.0.1:11211',
	}}

# Logging
LOGGING = {
	'version': 1,
	'disable_existing_loggers': False,

	'handlers': {
		'file': {
			'level': 'INFO',
			'class': 'logging.FileHandler',
			'filename': os.path.join(BASE_DIR, 'kamville.log')
		},
	},
	'loggers': {
		'django': {
			'handlers': ['file'],
			'level': 'INFO',
			'propagate': True,
		},
	}
}
