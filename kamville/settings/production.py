# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/
import os
import sys
import urllib.parse as urlparse
from kamville.settings.base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False


ALLOWED_HOSTS = ['*']


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
# Register database schemes in URLs.
urlparse.uses_netloc.append('mysql')
try:

    # Check to make sure DATABASES is set in settings.py file.
    # If not default to {}

    if 'DATABASES' not in locals():
        DATABASES = {}

    if 'DATABASE_URL' in os.environ:
        url = urlparse.urlparse(os.environ['DATABASE_URL'])
        print('url', url)

        # Ensure default database exists.
        DATABASES['default'] = DATABASES.get('default', {})

        # Update with environment configuration.
        DATABASES['default'].update({
            'NAME': 'vwpq30yi7imlvrq4',
            'USER': 'fo8m7uras9kdvmvu',
            'PASSWORD': 's28tze2ceiondnng',
            'HOST': 'g9fej9rujq0yt0cd.cbetxkdyhwsb.us-east-1.rds.amazonaws.com',
            'PORT': '3306',
            'OPTIONS': {
                "init_command": "SET foreign_key_checks = 1;",
            },
            'ENGINE': 'django.db.backends.mysql',
        })

        if url.scheme == 'mysql':
            DATABASES['default']['ENGINE'] = 'django.db.backends.mysql'
except Exception:
    print('Unexpected error:', sys.exc_info())


COMPRESS_ENABLED = os.environ.get('COMPRESS_ENABLED', False)

