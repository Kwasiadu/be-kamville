import sys

RUNNING_DEVSERVER = (len(sys.argv) > 1 and sys.argv[1] == 'runserver')

if RUNNING_DEVSERVER:
    from kamville.settings.local import *
else:
    from kamville.settings.production import *
