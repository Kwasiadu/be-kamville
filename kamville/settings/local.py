# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

from kamville.settings.base import *


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'kamville_db',
        'USER': 'kamville',
        'PASSWORD': os.environ.get("KAMVILLE_DB_PASSWORD", "Jumiamustdie2019"),
        'OPTIONS': {
             "init_command": "SET foreign_key_checks = 1;",
             },
    }
}

COMPRESS_ENABLED = os.environ.get('COMPRESS_ENABLED', False)
