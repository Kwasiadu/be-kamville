"""kamville URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URL conf
    1. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from kamville.utils import presigned, translate_to_french
from kamville.emails import kamville_mail


urlpatterns = [
    path('api/users/', include('users.urls')),
    path('api/sellers/', include('sellers.urls')),
    path('api/customers/', include('customers.urls')),
    path('api/addresses/', include('addresses.urls')),
    path('api/categories/', include('categories.urls')),
    path('api/parentcategories/', include('parent_categories.urls')),
    path('api/classifieds/', include('classifieds.urls')),
    path('api/filters/', include('filters.urls')),
    path('api/orders/items/', include('orderitems.urls')),
    path('api/orders/', include('orders.urls')),
    path('api/payments/', include('payments.urls')),
    path('api/products/', include('products.urls')),
    path('api/rentals/', include('rentals.urls')),
    path('api/reviews/', include('reviews.urls')),
    path('api/geolocation/', include('geolocation.urls')),
    path('api/amenities/', include('amenities.urls')),
    path('kamville_mail/', kamville_mail, name='kamville_mail'),

    path('get_presigned_AWS_url/', presigned, name='presigned'),
    path('translate_to_french/', translate_to_french,
         name='translate_to_french'),
    path('', include('auth0authorization.urls'))
]
