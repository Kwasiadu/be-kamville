import jwt
from django.http import JsonResponse
import boto3
import json
import sys
import os

from django.contrib.auth import authenticate
from functools import wraps
from django.views.decorators.csrf import csrf_exempt

import logging

# Get an instance of a logger
logger = logging.getLogger('django')


@csrf_exempt
def presigned(request):
    # manually set aws credentials so this works on ec2"
    s3 = boto3.client('s3',
                      aws_access_key_id='AKIARELIQRMA2ZF2DK3V',
                      aws_secret_access_key='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',
                      region_name='us-east-1',
                      )
    body = json.loads(request.body.decode('utf-8'))
    client_method = ''
    params = {}
    if body['operation'] == 'create':
        client_method = 'put_object'
        params = {
            'Bucket': 'kamvillee',
            'Key': body['folder'] + body['filename'],
            "ContentType": body['type'],
            "ACL": body['acl'],
        }

    elif body['operation'] == 'delete':
        client_method = 'delete_object'
        params = {
            'Bucket': 'kamvillee',
            'Key': body['folder'] + body['filename'],
        }
    url = s3.generate_presigned_url(
        ClientMethod=client_method,
        Params=params,
        ExpiresIn=3600)

    return JsonResponse({'url': url})


@csrf_exempt
def translate_to_french(request):
    translate = boto3.client(service_name="translate")
    body = json.loads(request.body.decode('utf-8'))
    response = translate.translate_text(Text=body[0]['text'],
                                        SourceLanguageCode="en",
                                        TargetLanguageCode="fr")
    return JsonResponse(response)


def get_user_email(request):
    # TODO: Figure out how to get email from token
    try:
        token = get_token_auth_header(request)
        if token:
            unverified_claims = jwt.get_unverified_claims(token)
            token_scopes = unverified_claims["scope"].split()
            for token_scope in token_scopes:
                if token_scope.startswith('email'):
                    user_email = token_scope.split(':')[1]
                    return user_email

    except Exception as e:
        print(e)


def jwt_get_username_from_payload_handler(payload):
    username = payload.get('sub').mreplace('|', '.')
    authenticate(remote_user=username)
    return username


def generate_403_response():
    response = JsonResponse(
        {'message': 'You don\'t have access to this resource'})
    response.status_code = 403
    return response


def requires_scope(required_scope):
    """Determines if the required scope is present in the Access Token
    Args:
        required_scope (str): The scope required to access the resource
    """
    def require_scope(f):

        @wraps(f)
        def decorated(*args, **kwargs):
            request = args[1]
            if has_required_scope(request, required_scope):
                return f(*args, **kwargs)
            return generate_403_response()

        return decorated
    return require_scope


def has_required_scope(request, required_scope):
    try:
        token = get_token_auth_header(request)
        if token and token != 'undefined':
            unverified_claims = jwt.get_unverified_claims(token)
            token_scopes = unverified_claims["scope"].split()
            for token_scope in token_scopes:
                if token_scope == required_scope:
                    return True
    except Exception as e:
        print(e)
    return False


def is_owner_or_has_required_scope(owner_fields,
                                   required_scope='write:all'):
    """Determines if the required scope is present in the Access Token
    Args:
        owner_fields ([]): An array of sequential fields of the listing object
        that must be accessed to get the right owner value
        required_scope (str): The scope required to access the resource

    """

    def check_field(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            view_obj = args[0]
            request = args[1]
            owner = view_obj.get_object()
            user = request.data
            for field in owner_fields:
                if owner:
                    try:
                        owner = getattr(owner, field)
                        user = user.get(field)
                        if not user:
                            user = kwargs.get(field)
                    except Exception as e:
                        logger.error("Failed to retrieve "
                                     "foreign key object in utils.py\n", e)
                        break
                else:
                    break
            if (user and user == owner) or \
                    has_required_scope(request, required_scope):
                return f(*args, **kwargs)

            return generate_403_response()

        return decorated
    return check_field


def get_token_auth_header(request):
    """Obtains the Access Token from the Authorization Header
    """
    token = request.META.get("HTTP_AUTH0", None)
    running_devserver = (len(sys.argv) > 1 and (sys.argv[1] == 'runserver'))
    running_testserver = (len(sys.argv) > 1 and (sys.argv[1] == 'test'))

    if not token or token == 'null':
        if running_devserver or running_testserver:
            is_admin = request.query_params.get('admin', None)
            if is_admin == 'true':
                token = os.environ.get("TEST_ADMIN_TOKEN", None)
            elif is_admin == 'false':
                token = os.environ.get("TEST_TOKEN", None)
    return token


def get_first(response, field, listing_id=1):
    for listing in response.data:
        if listing['id'] == listing_id:
            return listing[field]
