from rest_framework.routers import DefaultRouter
from rest_framework_extensions.routers import NestedRouterMixin

from users.views import UserViewSet
from sellers.views import SellerViewSet
from products.views import ProductViewSet

router = DefaultRouter()

router.register('users', UserViewSet)
router.register('sellers', SellerViewSet)


class NestedDefaultRouter(NestedRouterMixin, DefaultRouter):
    pass

router = NestedDefaultRouter()

users_router = router.register('users', UserViewSet)
users_router.register(
    'sellers',
    SellerViewSet,
    basename='user-seller',
    parents_query_lookups=['user'])\
    .register(
    'products',
    ProductViewSet,
    base_name='user-seller-products',
    parents_query_lookups=['seller__user', 'seller'])
