from rest_framework import generics
from kamville.utils import requires_scope
from amenities.models import Amenities
from amenities.serializers import AmenitiesSerializer


class AmenitiesList(generics.ListCreateAPIView):
    queryset = Amenities.objects.all()
    serializer_class = AmenitiesSerializer

    # @requires_scope('write:all')
    def post(self, request, *args, **kwargs):
        return super(AmenitiesList, self).post(request, *args, **kwargs)


class AmenitiesDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Amenities.objects.all().order_by('id')
    serializer_class = AmenitiesSerializer

    # @requires_scope('write:all')
    def post(self, request, *args, **kwargs):
        return super(AmenitiesDetail, self).put(request, *args, **kwargs)

    # @requires_scope('write:all')
    def put(self, request, *args, **kwargs):
        return super(AmenitiesDetail, self).put(request, *args, **kwargs)

    # @requires_scope('write:all')
    def delete(self, request, *args, **kwargs):
        return super(AmenitiesDetail, self).delete(request, *args, **kwargs)
