from django.db import models


# Create your models here.
class Amenities(models.Model):
	bedroom = models.IntegerField(default=0)
	bathroom = models.IntegerField(default=0)
	dishwasher = models.IntegerField(default=0)
	jacuzzi = models.IntegerField(default=0)
	television = models.IntegerField(default=0)
	barbeque = models.IntegerField(default=0)
	microwave = models.IntegerField(default=0)
	kitchen = models.IntegerField(default=0)

