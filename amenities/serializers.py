from amenities.models import Amenities
from rest_framework import serializers


class AmenitiesSerializer(serializers.ModelSerializer):
	class Meta:
		model = Amenities

		fields = ('id', 'bedroom', 'bathroom', 'dishwasher', 'jacuzzi',
				  'television', 'barbeque', 'microwave', 'kitchen')
