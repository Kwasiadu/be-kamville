from django.urls import path
from amenities import views

urlpatterns = [
    path('<int:pk>/', views.AmenitiesDetail.as_view()),
    path('', views.AmenitiesList.as_view()),
]
