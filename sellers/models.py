from django.db import models
from users.models import User
from addresses.models import Address

# TODO: fix amazon s3 cors issue
SELLER_URL = 'https://kamvillee.s3.amazonaws.com/static/img/sellers/'


# Focuses on vendors
class Seller(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='seller', on_delete=models.CASCADE)
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=32, blank=True)
    city = models.CharField(max_length=32, blank=True)
    image_name = models.CharField(max_length=64, blank=True)

    def __str__(self):
        return self.name + " -- " + self.email

    @property
    def image_url(self):
        return SELLER_URL + self.image_name

    @property
    def image_folder(self):
        return SELLER_URL


