from rest_framework.generics import \
    ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.decorators import action
from rest_framework.response import Response

from sellers.models import Seller
from sellers.serializers import SellerSerializer
from sellers.filters import SellerFilter


class SellerList(ListCreateAPIView):
    queryset = Seller.objects.all()
    serializer_class = SellerSerializer
    filter_class = SellerFilter

    def list(self, request, *args, **kwargs):
        return super(SellerList, self).list(request, *args, **kwargs)


class SellerDetail(RetrieveUpdateDestroyAPIView):
    queryset = Seller.objects.all()
    serializer_class = SellerSerializer


class SellerDetailWithProducts(RetrieveUpdateDestroyAPIView):
    serializer_class = SellerSerializer
    queryset = Seller.objects.all()

    @action(detail=True)
    def products(self):
        """
        Returns a list of all the products that the seller sells
        :return:
        """
        seller = self.get_object()
        products = seller.products.all()
        return Response([product.short_name for product in products])
