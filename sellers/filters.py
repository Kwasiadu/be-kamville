from django_filters import rest_framework as filters
from django.db.models import Q

from sellers.models import Seller


class SellerFilter(filters.FilterSet):
    class Meta:
        model = Seller
        fields = ()

    @property
    def qs(self):
        query = self.request.query_params.get('query', '')
        return Seller.objects.filter(Q(email__icontains=query) | Q(
            city__icontains=query))
