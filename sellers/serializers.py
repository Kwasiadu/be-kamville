from rest_framework import serializers

from sellers.models import Seller
from orderitems.serializers import OrderItemSerializer
from products.serializers import ProductSerializer


class SellerSerializer(serializers.ModelSerializer):
    seller_order_items = OrderItemSerializer(read_only=True, many=True)
    products = ProductSerializer(read_only=True, many=True)

    class Meta:
        model = Seller

        fields = ('id', 'email', 'name', 'city', 'image_name',
                  'products', 'seller_order_items',
                  'image_url', 'image_folder',)


