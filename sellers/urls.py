from django.urls import path
from sellers.views import SellerList, SellerDetail, SellerDetailWithProducts

urlpatterns = [
    path('<str:pk>/', SellerDetail.as_view()),
    path('<str:pk>/products', SellerDetailWithProducts.as_view()),
    path('', SellerList.as_view()),
]
