from django.db import models
from django.utils.translation import gettext_lazy as _

from customers.models import Customer
from addresses.models import Address

months_to_return_order_by = 6


class OrderStatus(models.TextChoices):
    INCOMPLETE = "I", _("Incomplete")
    COMPLETE = "C", _("Complete")


class Payment(models.TextChoices):
    CASH = "C", _("Cash")
    MTN = "MTN", _("MTN payment")


class DeliveryMethod(models.TextChoices):
    DELIVER = "DL", _("Deliver to customer")
    PICKUP = "PU", _("Pickup from store")


class Order(models.Model):
    id = models.AutoField(primary_key=True)
    koid = models.CharField(max_length=32, blank=True)
    customer = models.ForeignKey(Customer, related_name='orders',
                                 to_field="email",
                                 on_delete=models.CASCADE)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)
    total_price = models.DecimalField(max_digits=16, decimal_places=2, default=0)
    item_count = models.IntegerField(default=0)
    delivery_method = models.CharField(
        max_length=2,
        choices=DeliveryMethod.choices,
        default=DeliveryMethod.PICKUP,
        blank=True,
        null=True)
    status = models.CharField(
        max_length=8,
        choices=OrderStatus.choices,
        default=OrderStatus.COMPLETE,
        blank=True,
        null=True,
    )
    payment_method = models.CharField(
        max_length=5,
        choices=Payment.choices,
        default=Payment.CASH,
        blank=True,
        null=True,
    )
    shipping_address = models.ForeignKey(Address, related_name='order_shipping',
                                         on_delete=models.SET_NULL, blank=True,
                                         null=True)

    billing_address = models.ForeignKey(Address, related_name='order_billing',
                                        on_delete=models.SET_NULL, blank=True,
                                        null=True)

    def __str__(self):
        return self.customer.email + " " + self.koid


