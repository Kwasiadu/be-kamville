from orders.models import Order
from orderitems.serializers import OrderItemSerializer
from rest_framework import serializers


class OrderSerializer(serializers.ModelSerializer):
    # TODO: fix order_items list being empty when sent to client
    order_items = OrderItemSerializer(read_only=True, many=True)

    class Meta:
        model = Order

        fields = ('id', 'billing_address', 'created_at', 'customer',
                  'delivery_method', 'item_count', 'koid', 'order_items',
                  'payment_method', 'status',
                  'total_price',)
