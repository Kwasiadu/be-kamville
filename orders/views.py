from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import NotFound
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from orders.models import Order, DeliveryMethod, Payment, OrderStatus
from orders.serializers import OrderSerializer
from orders.filters import OrderFilter
from customers.models import Customer
from addresses.models import Address
from products.models import Product, PRODUCTS_URL

@api_view(['POST'])
def add_order_items(request):
    print('user', request.user)
    data = request.data

    customer = Customer.objects.get(email=request.user)
    shipping_address = Address.objects.get(id=Customer.addresses[0])
    billing_address = Address.objects.get(id=Customer.addresses[0])

    order_items = data['orderItems']

    if order_items and len(order_items) == 0:
        return Response({
            'detail': 'No Order Items',
            'status': status.HTTP_400_BAD_REQUEST
        })
    else:
        # Create Order
        order = Order.objects.create(
            customer=customer,
            delivery_method=DeliveryMethod.PICKUP,
            payment_method=Payment.CASH,
            order_status=OrderStatus.INCOMPLETE,
            total_price=data['total_price'],
            shipping_address=shipping_address,
            billing_address=billing_address,
        )

        # Create order items
        for order_item in order_items:
            product = Product.objects.get(id=order_item['product'])

            item = order_item.objects.create(
                product=product,
                order=order,
                seller=product.seller,
                customer=customer,
                name=product.name,
                qty=order_item['qty'],
                price=order_item['total_price'],
                shipping_cost=order_item['shipping_cost'],
                image_url=PRODUCTS_URL + product.image_folder + product.image1
            )

            # (4) Update Stock
            product.count -= item.qty
            product.purchased_count += item.qty
            product.save()

        serializer = OrderSerializer(order, many=False)
        return Response(serializer.data)


class OrderList(generics.ListCreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    filter_class = OrderFilter


class OrderDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = (IsAuthenticated,)


class CurrentOrder(generics.RetrieveAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def get_object(self):
        try:
            return self.queryset.all().filter(status="I").latest('created_at')
        except Order.DoesNotExist:
            # TODO: figure out better way to handle no current order/empty cart
            raise NotFound(detail="Error 404, no current order", code=404)

