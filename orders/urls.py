from django.urls import path
from orders import views

urlpatterns = [
    path('<int:pk>/', views.OrderDetail.as_view()),
    path('current/', views.CurrentOrder.as_view()),
    path('', views.OrderList.as_view()),
]
