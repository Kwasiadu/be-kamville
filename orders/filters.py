from django_filters import rest_framework as filters
from orders.models import Order


class OrderFilter(filters.FilterSet):
    customer = filters.Filter(field_name="customer__email")

    class Meta:
        model = Order
        fields = ('customer',)

    @property
    def qs(self):
        customer = self.request.query_params.get('customer', '')
        return Order.objects.filter(customer__email__icontains=customer)

