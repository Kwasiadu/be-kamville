from django.db import models
from users.models import User
from products.models import Product

REVIEW_URL = 'https://kamvillee.s3.amazonaws.com/static/img/reviews/'


class Review(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=128)
    rating = models.IntegerField()
    date = models.DateField(auto_now_add=True)
    image = models.CharField(max_length=64, blank=True, null=True)
    content = models.TextField()
    helpful_count = models.IntegerField(default=0)
    abuse_count = models.IntegerField(default=0)
    verified_purchase = models.BooleanField(default=False)
    author = models.ForeignKey(User, to_field='email', related_name='reviews',
                               on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='reviews',
                                on_delete=models.CASCADE)
    publish = models.BooleanField(verbose_name="Publish listing?", default=True)

    def __str__(self):
        return self.title

    @property
    def image_url(self):
        if self.image:
            return REVIEW_URL + self.image
        else:
            return ""
