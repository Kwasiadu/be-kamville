from django_filters import rest_framework as filters
from reviews.models import Review


class ReviewFilter(filters.FilterSet):
    query = filters.Filter(field_name="product", lookup_expr='icontains')

    class Meta:
        model = Review
        fields = ('query',)

    @property
    def qs(self):
        query = self.request.query_params.get('query', '')
        return Review.objects.filter(product__name__icontains=query) \
            .order_by('date')
