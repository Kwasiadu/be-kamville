from rest_framework import generics
from kamville.utils import requires_scope
from reviews.models import Review
from reviews.serializers import ReviewSerializer
from reviews.filters import ReviewFilter


class ReviewList(generics.ListCreateAPIView):
    queryset = Review.objects.all().order_by('-helpful_count')
    serializer_class = ReviewSerializer
    filter_class = ReviewFilter

    # @requires_scope('write:all')
    def post(self, request, *args, **kwargs):
        return super(ReviewList, self).post(request, *args, **kwargs)


class ReviewDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Review.objects.all().order_by('id')
    serializer_class = ReviewSerializer

    # @requires_scope('write:all')
    def put(self, request, *args, **kwargs):
        return super(ReviewDetail, self).put(request, *args, **kwargs)

    # @requires_scope('write:all')
    def delete(self, request, *args, **kwargs):
        return super(ReviewDetail, self).delete(request, *args, **kwargs)
