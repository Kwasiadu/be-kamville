from django.urls import path
from reviews import views

urlpatterns = [
    path('<int:pk>/', views.ReviewDetail.as_view()),
    path('', views.ReviewList.as_view()),
]
