from reviews.models import Review
from rest_framework import serializers


class ReviewSerializer(serializers.ModelSerializer):
    author_name = serializers.CharField(source="author.nickname", read_only=True)
    author_image_url = serializers.CharField(source="author.image_url",
                                             read_only=True)

    class Meta:
        model = Review

        fields = ('id', 'rating', 'date', 'title', 'content',
                  'helpful_count',
                  'image', 'image_url', 'verified_purchase', 'author_name',
                  'author_image_url', 'author', 'abuse_count', 'product')
