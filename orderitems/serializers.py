from orderitems.models import OrderItem
from products.serializers import ProductSerializer


from rest_framework import serializers


class OrderItemSerializer(serializers.ModelSerializer):
    product = ProductSerializer(read_only=True)

    class Meta:
        model = OrderItem

        fields = ('id', 'created_at', 'customer',
                  'delivered_at', 'image_url',
                  'name', 'order', 'product', 'qty', 'return_by',
                  'seller', 'shipping_cost', 'status', 'tax',
                  'total_price',
                  )


