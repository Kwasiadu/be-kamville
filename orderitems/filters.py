from django_filters import rest_framework as filters
from orderitems.models import OrderItem


class OrderItemFilter(filters.FilterSet):

    class Meta:
        model = OrderItem
        fields = ('seller',)


# class IsOwnerFilterBackend(filters.BaseFilterBackend):
#     """
#     Filter that only allows users to see their own objects.
#     """
#     def filter_queryset(self, request, queryset, view):
#         return queryset.filter(owner=request.user)
