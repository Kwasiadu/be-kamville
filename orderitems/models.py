import decimal

from django.db import models
from django.utils.translation import gettext_lazy as _

from products.models import Product
from orders.models import Order
from users.models import User
from sellers.models import Seller

VAT_RATE = 0.15


class DeliveryStatus(models.TextChoices):
    IN_CART = "IC", _("In cart")
    RECEIVED = "OR", _("Order received")
    SHIPPED = "SH", _("Shipped")
    DELIVERED = "DL", _("Delivered")
    CANCELLED = "CA", _("Cancelled")
    RETURN_REQUESTED = "RR", _("Return requested")
    RETURNED = "RE", _("Returned")


class OrderItem(models.Model):
    id = models.AutoField(primary_key=True, editable=False)
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    order = models.ForeignKey(Order, on_delete=models.SET_NULL, null=True,
                              related_name='order_items')
    customer = models.ForeignKey(User, to_field='email', null=True,
                                 related_name='customer_order_items',
                                 on_delete=models.SET_NULL)
    seller = models.ForeignKey(Seller, null=True,
                                 related_name='seller_order_items',
                                 on_delete=models.SET_NULL)
    name = models.CharField(max_length=200, null=True, blank=True)
    qty = models.PositiveIntegerField()
    price = models.DecimalField(decimal_places=2, max_digits=16,
                                null=True, blank=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)
    return_by = models.DateField(null=True, blank=True)
    delivered_at = models.DateField(auto_now_add=False, null=True, blank=True)
    shipping_cost = models.DecimalField(max_digits=16, decimal_places=2,
                                        null=True, blank=True)
    image_url = models.CharField(max_length=200, null=True, blank=True)
    status = models.CharField(
        max_length=16,
        choices=DeliveryStatus.choices,
        default=DeliveryStatus.RECEIVED,
    )

    def __str__(self):
        return self.product.name

    def tax(self):
        return self.price * decimal.Decimal(VAT_RATE)

    def total_price(self):
        return self.price * self.qty + self.tax() + self.shipping_cost

#     def save(self, *args, **kwargs):
#         self.price = self.product_related.price
#         self.seller = self.product_related.seller
#         self.date = self.order_related.date
#         tax = self.product_related.list_price * Decimal(VAT_RATE)
#         self.tax = round(tax, 2)
#         self.shipping_cost = self.product_related.shipping_cost
#         self.total_price = self.price * self.qty + self.tax + self.shipping_cost
#         if self.order_related.status == "C":
#             self.product_related.purchased_count += 1
#             self.product_related.total_revenue += self.total_price
#             self.product_related.save()
#
#         if self.delivery_status == "return requested":
#             self.product_related.returned_count += 1
#         super(OrderItem, self).save(*args, **kwargs)
#
#
# @receiver(post_delete, sender=OrderItem)
# def update_order(sender, instance, *args, **kwargs):
#     instance.order_related.save()
#
#
# @receiver(post_delete, sender=OrderItem)
# def update_product(sender, instance, *args, **kwargs):
#     instance.order_product_related.purchased_count -= 1
#     instance.order_product_related.total_revenue_count -= instance.total_price
#     instance.order_product_related.save()
