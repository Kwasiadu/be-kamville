from rest_framework import generics
from orderitems.models import OrderItem
from orderitems.serializers import OrderItemSerializer
from orderitems.filters import OrderItemFilter


class OrderItemList(generics.ListCreateAPIView):
    queryset = OrderItem.objects.all().order_by('created_at')
    serializer_class = OrderItemSerializer
    filter_class = OrderItemFilter


class OrderItemDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = OrderItem.objects.all().order_by('created_at')
    serializer_class = OrderItemSerializer

