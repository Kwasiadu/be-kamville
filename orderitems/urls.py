from django.urls import path
from orderitems import views

urlpatterns = [
    path('<int:pk>/', views.OrderItemDetail.as_view()),
    path('', views.OrderItemList.as_view()),
]
