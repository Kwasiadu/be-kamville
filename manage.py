#!/usr/bin/env python
import os
import sys
from django.core.management import execute_from_command_line
from django.core.handlers.wsgi import WSGIHandler

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "kamville.settings")
    execute_from_command_line(sys.argv)


application = WSGIHandler()
