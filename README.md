# INTRODUCTION	
This kamville's backend code

## REQUIRED COMPONENTS	
1. Python (tested with 3.7)

2. Mysql (tested with 5.7.17)

##Install Xcode
Download Xcode from https://itunes.apple.com/au/app/xcode/id497799835?mt=12

Install command line tools — `$ xcode-select --install`


## Install Homebrew
`$ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)”`

Make sure brew is up to date: `$ brew update`

Check for errors: `$ brew doctor`

If there are any errors, you will have to fix them before continuing

Add Homebrew’s location to your $PATH in your .bash_profile `$ export 
PATH="/usr/local/bin:$PATH”`


## Install Mysql
`$ brew install mysql`

To have launchd start mysql at login : `$ ln -sfv /usr/local/opt/mysql/*.plist ~/Library/LaunchAgents`

To load mysql immediately : `$ launchctl load ~/Library/LaunchAgents/homebrew.mxcl.mysql.plist`

Finally add the mysql directory to your PATH environment variable in .bash_profile :
`$ export MYSQL_PATH=/usr/local/Cellar/mysql/5.7.17`

(If you installed a different version, replace 5.7.17 with that version in /usr/local/Cellar/mysql/)

`$export PATH=$PATH:$MYSQL_PATH/bin`

## Install Python, pip, and virtualenv
Install python 3 if not already on system — `$ brew install python`

Install pip `$ sudo easy_install pip`

Install virtualenv  `$ pip install virtualenv`


# Setup Environment
Create a directory to store the project - `$ mkdir -p ~/src`

Navigate to project folder — `$ cd ~/src`

Clone  bridge repository from bitbucket — `$ git clone https://Kwasiadu@bitbucket.org/Kwasiadu/be-kamville.git`

Make virtualenv: `$ virtualenv venv`

Open virtualenv`$ source ~/src/venv/bin/activate`

(Fyi, to close virtual environment, use this command: `$ deactivate`)

Navigate to project: `$ cd ~/src/be-kamville`

Install requirements: `$ python3 -m pip install -r requirements.txt`

Sync databases: `$ python3 manage.py migrate`

# Run Project
`$ python3 manage.py runserver`
