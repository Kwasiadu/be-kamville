import sys
import os
import lob

sys.path.insert(0, os.path.abspath(__file__+'../../..'))

lob.api_key = os.environ.get("LOB_TEST_SK", None)
print("_______", lob.api_key)

# Create an address object

example_address = lob.Address.create(
    name='Kwasi Adu',
    description='Kwasi - Home',
    metadata={
        'group': 'Members'
    },
    address_line1='555 Walker Dr, #205',
    address_city='Mountain View',
    address_state='CA',
    address_country='US',
    address_zip='94043',
)

print("\n")
print("Address Response")
print("\n")
print("=======================================================")
print("\n")
print(example_address)
print("\n")
print("=======================================================")
print("\n")

# Creating a Postcard

example_postcard = lob.Postcard.create(
    description='Test Postcard',
    metadata={
        'campaign': 'Member welcome'
    },
    to_address=example_address,
    from_address=example_address,
    front="""
    <html>
        <head>
          <style>
            @font-face {
              font-family: 'Loved by the King';
              src: url('https://s3-us-west-2.amazonaws.com/lob-assets/LovedbytheKing.ttf');
            }
          </style>
        </head>
        <body><h1>Hi {{name}}</h1></body>
      </html>
    """,
    merge_variables={
        'name': example_address.name
    },
    back="<h1>Welcome to the club!</h1>"

)

print("Postcard Response")
print("\n")
print("=======================================================")
print("\n")
print(example_postcard)
print("\n")
print("=======================================================")
print("\n")




