import lob
import os

lob.api_key = os.environ.get("LOB_TEST_SK", None)

response = lob.USVerification.create(
  primary_line='deliverable',
  city='San Francisco',
  state='CA',
  zip_code='11111'
)


print(response)