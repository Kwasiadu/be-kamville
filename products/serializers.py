from products.models import Product
from rest_framework import serializers
from reviews.serializers import ReviewSerializer


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product

        fields = ('id', 'name', 'short_name', 'price', 'list_price', 'category',
                  'seller', 'publish', 'tag_line', 'color', 'count', 'brand',
                  'weight', 'weight_unit', 'model_no', 'KSIN', 'description',
                  'condition',
                  'date_listed', 'warranty', 'video_url', 'video_id',
                  'image_folder', 'searched_count', 'viewed_count',
                  'purchased_count', 'returned_count',
                  'images', 'avg_rating', 'no_of_ratings', 'total_revenue',
                  'image1', 'image2', 'image3', 'image4', 'image5',
                  )


class ProductWithReviewsSerializer(serializers.ModelSerializer):
    reviews = ReviewSerializer(read_only=True, many=True)

    class Meta:
        model = Product

        fields = ('id', 'name', 'short_name', 'price', 'list_price', 'category',
                  'seller', 'publish', 'tag_line', 'color', 'count',
                  'weight', 'weight_unit', 'model_no', 'KSIN', 'description',
                  'condition',
                  'date_listed', 'warranty', 'video_url', 'video_id',
                  'image_folder', 'searched_count', 'viewed_count',
                  'purchased_count', 'returned_count',
                  'images', 'reviews', 'avg_rating', 'no_of_ratings',
                  'image1', 'image2', 'image3', 'image4', 'image5',
                  )


class RecommendedProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product

        fields = ('id', 'name', 'short_name', 'price', 'list_price', 'category',
                  'seller', 'description', 'purchased_count',  'images',
                  'avg_rating', 'no_of_ratings',)


class UpdateProductAnalyticsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product

        fields = ('id', 'searched_count', 'viewed_count',
                  'purchased_count', 'returned_count',)
