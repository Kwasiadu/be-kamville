from rest_framework import generics
from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin

from products.models import Product
from products.serializers import ProductSerializer, \
    RecommendedProductSerializer, ProductWithReviewsSerializer, \
    UpdateProductAnalyticsSerializer
from products.filters import ProductFilter, RecommendedProductFilter
from kamville.pagination import StandardResultsSetPagination


class ProductViewSet(NestedViewSetMixin, ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductList(generics.ListCreateAPIView):
    queryset = Product.objects.all().order_by('id')
    serializer_class = ProductSerializer
    filter_class = ProductFilter
    pagination_class = StandardResultsSetPagination


class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all().order_by('id')
    serializer_class = ProductWithReviewsSerializer

    # @requires_scope('write:all')
    def delete(self, request, *args, **kwargs):
        return super(ProductDetail, self).delete(request, *args, **kwargs)


class RecommendedProductList(generics.ListAPIView):
    queryset = Product.objects.all()
    serializer_class = RecommendedProductSerializer
    filter_class = RecommendedProductFilter


class UpdateProductAnalytics(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all().order_by('id')
    serializer_class = UpdateProductAnalyticsSerializer

    def put(self, request, *args, **kwargs):
        print('put analytics')
        return super(UpdateProductAnalytics, self).put(request, *args, **kwargs)
