from django_filters import rest_framework as filters
from products.models import Product


class ProductFilter(filters.FilterSet):
    name = filters.Filter(field_name="name", lookup_expr='icontains')
    category = filters.Filter(field_name="category", lookup_expr='icontains')

    class Meta:
        model = Product
        fields = ('name', 'category')

    @property
    def qs(self):
        name = self.request.query_params.get('name', '')
        seller = self.request.query_params.get('seller', '')
        category = self.request.query_params.get('category', '')
        order_by = self.request.query_params.get('order_by', 'id') or 'id'
        category = "" if category == "All" else category
        return Product.objects.filter(name__icontains=name,
                                      seller__email__icontains=seller,
                                      category__parent__name__icontains=category) \
            .order_by(order_by)


class RecommendedProductFilter(filters.FilterSet):
    category = filters.Filter(field_name="category", lookup_expr='icontains')

    class Meta:
        model = Product
        fields = ('category',)

    @property
    def qs(self):
        category = self.request.query_params.get('category', '')
        category = "" if category == "All" else category
        return Product.objects \
            .filter(category__name__icontains=category) \
            .order_by('purchased_count')[:4]
#
