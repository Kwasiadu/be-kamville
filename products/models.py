from django.db import models
from django.db.models import Avg
from sellers.models import Seller
from categories.models import Category


PRODUCTS_URL = 'https://kamvillee.s3.amazonaws.com/static/img/products/'
DEFAULT_SHIPPING_COST = 5.00


class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=128)
    short_name = models.CharField(max_length=16)
    price = models.DecimalField(max_digits=16, decimal_places=2)
    list_price = models.DecimalField(max_digits=16, decimal_places=2)
    shipping_cost = models.DecimalField(max_digits=16, decimal_places=2,
                                        default=DEFAULT_SHIPPING_COST)
    tag_line = models.CharField(max_length=256)
    color = models.CharField(max_length=16, blank=True, null=True)
    count = models.IntegerField()
    brand = models.CharField(max_length=128, null=True, blank=True)
    weight = models.IntegerField(blank=True, null=True)
    weight_unit = models.CharField(max_length=8, blank=True, null=True)
    model_no = models.CharField(max_length=16, blank=True, null=True)
    KSIN = models.CharField(max_length=32, blank=True, null=True)
    description = models.TextField(max_length=128, blank=True, null=True)
    condition = models.CharField(max_length=16, blank=True, null=True)
    date_listed = models.DateField(auto_now_add=True, null=True, blank=True)
    warranty = models.TextField(blank=True, null=True)
    video_id = models.CharField(max_length=64, blank=True, null=True)
    image1 = models.CharField(max_length=64)
    image2 = models.CharField(max_length=64, blank=True)
    image3 = models.CharField(max_length=64, blank=True)
    image4 = models.CharField(max_length=64, blank=True)
    image5 = models.CharField(max_length=64, blank=True)
    # rating = models.DecimalField(max_digits=1, decimal_places=2)
    total_revenue = models.DecimalField(max_digits=24, decimal_places=2,
                                        default=0)
    searched_count = models.IntegerField(default=0)
    viewed_count = models.IntegerField(default=0)
    purchased_count = models.IntegerField(default=0)
    returned_count = models.IntegerField(default=0)

    category = models.ForeignKey(Category, related_name='products',
                                 to_field='name', on_delete=models.PROTECT)
    seller = models.ForeignKey(Seller, related_name='products', on_delete=models.CASCADE)
    publish = models.BooleanField(verbose_name="Publish listing?", default=True)

    # def save(self, *args, **kwargs):
    #     self.rating = self.reviews.all().aggregate(Avg('rating')) \
    #         ['rating__avg'] if self.reviews.all() else 0
    #     super(Product, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    @property
    def avg_rating(self):
        return self.reviews.all().aggregate(Avg('rating'))['rating__avg'] \
            if self.reviews.all() else 0

    @property
    def no_of_ratings(self):
        return self.reviews.count()

    @property
    def image_folder(self):
        return self.short_name.lower().replace(" ", "_") + "/"

    @property
    def images(self):
        image_list = []
        image_list.append(PRODUCTS_URL + self.image_folder + self.image1)

        if self.image2:
            image_list.append(PRODUCTS_URL + self.image_folder + self.image2)
        if self.image3:
            image_list.append(PRODUCTS_URL + self.image_folder + self.image3)
        if self.image4:
            image_list.append(PRODUCTS_URL + self.image_folder + self.image4)
        if self.image5:
            image_list.append(PRODUCTS_URL + self.image_folder + self.image5)
        return image_list

    @property
    def video_url(self):
        if self.video_id:
            return "https://www.youtube.com/embed/" + self.video_id + "?autoplay=1"



