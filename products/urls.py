from django.urls import path
from products import views

urlpatterns = [
    path('<int:pk>/analytics', views.UpdateProductAnalytics.as_view()),
    path('<int:pk>/', views.ProductDetail.as_view()),
    path('recommended', views.RecommendedProductList.as_view()),
    path('', views.ProductList.as_view()),
]
