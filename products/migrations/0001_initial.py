# Generated by Django 3.0 on 2023-02-08 07:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('categories', '0001_initial'),
        ('sellers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=128)),
                ('short_name', models.CharField(max_length=16)),
                ('price', models.DecimalField(decimal_places=2, max_digits=16)),
                ('list_price', models.DecimalField(decimal_places=2, max_digits=16)),
                ('shipping_cost', models.DecimalField(decimal_places=2, default=5.0, max_digits=16)),
                ('tag_line', models.CharField(max_length=256)),
                ('color', models.CharField(blank=True, max_length=16, null=True)),
                ('count', models.IntegerField()),
                ('brand', models.CharField(blank=True, max_length=128, null=True)),
                ('weight', models.IntegerField(blank=True, null=True)),
                ('weight_unit', models.CharField(blank=True, max_length=8, null=True)),
                ('model_no', models.CharField(blank=True, max_length=16, null=True)),
                ('KSIN', models.CharField(blank=True, max_length=32, null=True)),
                ('description', models.TextField(blank=True, max_length=128, null=True)),
                ('condition', models.CharField(blank=True, max_length=16, null=True)),
                ('date_listed', models.DateField(auto_now_add=True, null=True)),
                ('warranty', models.TextField(blank=True, null=True)),
                ('video_id', models.CharField(blank=True, max_length=64, null=True)),
                ('image1', models.CharField(max_length=64)),
                ('image2', models.CharField(blank=True, max_length=64)),
                ('image3', models.CharField(blank=True, max_length=64)),
                ('image4', models.CharField(blank=True, max_length=64)),
                ('image5', models.CharField(blank=True, max_length=64)),
                ('total_revenue', models.DecimalField(decimal_places=2, default=0, max_digits=24)),
                ('searched_count', models.IntegerField(default=0)),
                ('viewed_count', models.IntegerField(default=0)),
                ('purchased_count', models.IntegerField(default=0)),
                ('returned_count', models.IntegerField(default=0)),
                ('publish', models.BooleanField(default=True, verbose_name='Publish listing?')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='products', to='categories.Category', to_field='name')),
                ('seller', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='products', to='sellers.Seller')),
            ],
        ),
    ]
